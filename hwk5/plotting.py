from __future__ import division
import matplotlib.pyplot as plt
from math import *


def plot_err(pt_list1, pt_list2, pt_list3,  startx, endx, starty, endy, title):
    '''
    Function plots a piece-wise list of points of
    the form [(x_1, x_i ... ) (y_1, y_i ...]
    With plot limits startx, endx, starty, endy
     (TODO: get max/min x/y and use for plot
     parameters)
    :param pt_list: list of the form (x_list, y_list)
    :param startx: plot parameters
    :param endx:
    :param starty:
    :param endy:
    :return:
    '''

    plt.title(title)

    plt.plot(pt_list1[0], pt_list1[2],'co')
    plt.plot(pt_list1[0], pt_list1[2], 'c-', label="h = 0.2")

    plt.plot(pt_list2[0], pt_list2[2],'go')
    plt.plot(pt_list2[0], pt_list2[2], 'g--', label="h = 0.1")

    plt.plot(pt_list3[0], pt_list3[2],'bo')
    plt.plot(pt_list3[0], pt_list3[2], 'b-.', label= "h = 0.05")

    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='white')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.legend(loc='upper right')
    # Display graphs
    plt.show()



def plot_list3(f1, pt_list1, pt_list2, pt_list3,  startx, endx, starty, endy, title):
    '''
    Function plots a piece-wise list of points of
    the form [(x_1, x_i ... ) (y_1, y_i ...]
    With plot limits startx, endx, starty, endy
     (TODO: get max/min x/y and use for plot
     parameters)
    :param pt_list: list of the form (x_list, y_list)
    :param startx: plot parameters
    :param endx:
    :param starty:
    :param endy:
    :return:
    '''

    plt.title(title)
    plt.plot(pt_list1[0], pt_list1[1],'co')
    plt.plot(pt_list1[0], pt_list1[1], 'c-', label="h = 0.2")

    plt.plot(pt_list2[0], pt_list2[1],'go')
    plt.plot(pt_list2[0], pt_list2[1], 'g--', label="h = 0.1")

    plt.plot(pt_list3[0], pt_list3[1],'bo')
    plt.plot(pt_list3[0], pt_list3[1], 'b-.', label= "h = 0.05")

    # plot function
    increment = 1000
    step = (endx - startx)/increment
    # Compute lists of x and y coordinates for the two graphs
    xsteps = [startx + k*step for k in range(increment)]
    fx1 = [f1(t) for t in xsteps]
    plt.plot(xsteps, fx1, color='black', label = r'$\cos 2 \pi t$')

    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.legend(loc='lower right')
    # Display graphs
    plt.show()



def plot_funcs(funcs, startx, endx, labels, title):
    # plot function
    increment = 1000
    step = ( endx - startx )/increment
    for i in range(len(funcs)):
         # Compute lists of x and y coordinates for the two graphs
        xsteps = [startx + k*step for k in range(increment+1)]
        fx1 = [funcs[i](t) for t in xsteps]
        plt.title(title)
        plt.plot(xsteps, fx1, color='blue', label = labels[i])

    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.legend(loc='lower right')
    plt.show()


def plot_func(f1, startx, endx, label1, title):
    # plot function
    increment = 1000
    step = ( endx - startx )/increment
    # Compute lists of x and y coordinates for the two graphs
    xsteps = [startx + k*step for k in range(increment+1)]
    fx1 = [f1(t) for t in xsteps]
    plt.title(title)
    plt.plot(xsteps, fx1, color='blue', label = label1)
    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0.8, color='white')
    plt.legend(loc='upper left')
    plt.show()


def plot_list2(pt_list, startx, endx, starty, endy):
    '''
    Function plots a piece-wise list of points of
    the form [(x_1, x_i ... ) (y_1, y_i ...]
    With plot limits startx, endx, starty, endy
     (TODO: get max/min x/y and use for plot
     parameters)
    :param pt_list: list of the form (x_list, y_list)
    :param startx: plot parameters
    :param endx:
    :param starty:
    :param endy:
    :return:
    '''
    plt.plot(pt_list[0], pt_list[1], 'ro')
    # plt.axis([startx, endx, starty, endy], 3/4)
    # draw axes centered at the origin
    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.legend(loc='lower right')
    # Display graphs
    plt.show()


print("plotting")