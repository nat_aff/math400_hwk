from __future__ import division
import numpy as np
from math import *

#import numdifftools as ndt

__author__ = 'nathanael'

# Some matrix calculations:
A = np.array([[1,2],[3,4]])
A
A = np.arange(A).reshape(2,2)
A = A.reshape(2,2)



def f0(vec):
    return 3*vec[0]**2 - vec[1]**2

def f1(vec):
    return 3*vec[0]*vec[1]**2 - vec[0]**2 - 1

def j11(vec):
    return 6*vec[0]

def j12(vec):
    return -2*vec[1]

def j21(vec):
    return 3*vec[1]**2 - 2*vec[0]

def j22(vec):
    return 6*vec[0]*vec[1]


def newton2d(f1s, jfun, x0, max_iter, epsilon):
    #x1 = x0
    x1 = (10000, 0)
    prev = x0
    iter = 0
    while fabs(vec_inf_norm(x1) - vec_inf_norm(prev)) > epsilon and iter < max_iter:
        delta_x0 = delta_x(jfun, funs, x0)
        x1 = (x0[0] + delta_x0[0], x0[1] + delta_x0[1])
        print "xn+1 := ", x1
        prev = x0
        x0 = x1
        iter += 1
    return (x1, iter)



def delta_x(jfun, funs, x0):
    mat = np.matrix([[jfun[0](x0), jfun[1](x0)],[jfun[2](x0),jfun[3](x0)]])
    inv = mat.getI()
    fxs = np.array([[funs[0](x0)], [funs[1](x0)]])
    fxs = fxs *-1
    #print "inv mat: ", mat,  inv, fxs
    result = inv*fxs
    result = (result.item(0), result.item(1))
    return result



def vec_inf_norm(int_list):
    '''
    Get
    :param int_list:
    :return:
    '''
    list_max = max(max(int_list), min(int_list), key=abs)
    return abs(list_max)



## test delta_x
#def newtons2d(f1s, jfun, x0, max_iter, epsilon):
jfun = (j11, j12, j21, j22)
funs = (f0, f1)
x0 = (1, 1)
res = newton2d(funs, jfun, x0, 6, 0.000000005)
res


#== Test result for first iteration
jfun = (j11, j12, j21, j22)
funs = (f0, f1)
x0 = (1, 1)
res = delta_x(jfun, funs, x0)
type(res[0])




