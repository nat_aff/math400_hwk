from __future__ import division
from math import *
import plotting as plts

def f1(x,t):
    return t - 2*x

def f1_sol(t):
    res = (5/4)* (e**(-2*t)) + (1/4)*(2*t -1)
    return res

def f2(x):
    return -4*(pi**2)*x

def f2_sol(t):
    return cos(2*pi*t)

def rel_err(func, t, x):
    return fabs(func(t) - x)/fabs(func(t))

def eulers(f1, sol, x0, t0, h, stop):
    xlist = []
    ylist = []
    rlist = []
    xlist.append(t0)
    ylist.append(x0)
    rlist.append(0)
    while (t0 < stop):
        m = f1(x0, t0 -h )
        x1 = x0 + h*m
        t1 = t0 + h
        #print x1, t1
        xlist.append(t1)
        ylist.append(x1)
        rlist.append(rel_err(sol, t1, x1))
        x0 = x1
        t0 = t1
    return (xlist, ylist, rlist)


def eulers_mod(f1, sol, x0, t0, h, stop):
    xlist = []
    ylist = []
    rlist = []
    xlist.append(t0)
    ylist.append(x0)
    rlist.append(0)
    while (t0 < stop ):
        x1p = x0 + h*f1(x0, t0)
        t1 = t0 + h
        x1 = x0 + (h/2)*( f1(x0, t0) + f1(x1p, t1))
        #print x1, t1
        xlist.append(t1)
        ylist.append(x1)
        rlist.append(rel_err(sol, t1, x1))
        x0 = x1
        t0 = t1
    return (xlist, ylist, rlist)


def rk4(f1, sol, x0, t0, h, stop):
    xlist = [], ylist = [], rlist = []
    xlist.append(t0)
    ylist.append(x0)
    rlist.append(0)
    while(t0 < stop):
        k1 = f1(x0, t0)
        k2 = f1(x0 + (h/2)*k1 , t0 + h/2)
        k3 = f1( x0 + (h/2)*k2 , t0 + h/2)
        k4 = f1( x0 + h*k3, t0 + h)
        x1 = x0 + (h/6)*(k1 + 2*k2 + 2*k3 + k4)
        t1 = t0 + h
        #print x1, t1
        xlist.append(t1)
        ylist.append(x1)
        rlist.append(rel_err(sol, t1, x1))
        x0 = x1
        t0 = t1
    return (xlist, ylist, rlist)


def eulers2(f2, sol, t0, x0, y0, h, stop):
    tlist = []; xlist = []; rlist = []
    tlist.append(t0)
    xlist.append(x0)
    rlist.append(0)
    while(t0 < stop):
        t1 = t0 + h
        x1 = x0 + h*y0
        y1 = y0 + h*f2(x0)
        tlist.append(t1)
        xlist.append(x1)
        rlist.append(rel_err(sol, t1, x1))
        x0 = x1
        t0 = t1
        y0 = y1
        print t0, x0, y0
    return(tlist, xlist, rlist)


#---- Problem 1

e1 = eulers(f1, f1_sol, 1, 0, .2, 1.8)
e2 = eulers(f1, f1_sol, 1, 0, .1, 2)
e3 = eulers(f1, f1_sol, 1, 0, .05, 2)

em1 = eulers_mod(f1,f1_sol, 1, 0, .2, 1.8)
em2 = eulers_mod(f1, f1_sol,1, 0, .1, 2)
em3 = eulers_mod(f1, f1_sol,1, 0, 0.05, 2)


r1 = rk4(f1, f1_sol, 1, 0, .2, 1.8)
r2 = rk4(f1,f1_sol, 1, 0, .1, 2)
r3 = rk4(f1, f1_sol,1, 0, .05, 2)

reload(plts)
# plot def plot_list(pt_list, startx, endx, starty, endy):
plts.plot_list3(f1_sol, e1, e2, e3, 0, 2, 0, 1, "Euler's Method with steps {0.2, 0.1, 0.05}")

# Modified Euler's
plts.plot_list3(f1_sol, em1, em2, em3, 0, 2, 0, 1, "Modified Euler's Method with steps {0.2, 0.1, 0.05}")
# RK
plts.plot_list3(f1_sol, r1, r2, r3, 0, 2, 0, 1, "Runge-Kutta with steps {0.2, 0.1, 0.05}")

# Plot relative error

plts.plot_err( e1, e2, e3, 0, 2, 0, 1, "Relative error for Euler's Method with steps {0.2, 0.1, 0.05}")

plts.plot_err( em1, em2, em3, 0, 2, 0, 1, "Relative error for Modified Euler's with steps {0.2, 0.1, 0.05}")

plts.plot_err( r1, r2, r3, 0, 2, 0, 1, "Relative error for Runge-Kutta with steps {0.2, 0.1, 0.05}")



#---- Problem 2 tests

t0 = 0; x0 = 1; y0 = 0
h = .2
e21 = eulers2(f2, f2_sol, t0, x0, y0, 0.2, 1.2)
e22 = eulers2(f2, f2_sol, t0, x0, y0, 0.1, 1.2)
e23 = eulers2(f2, f2_sol, t0, x0, y0, 0.05, 1.2)

filename1 = "euler1.txt"
filename2 = "euler2.txt"
filename3 = "euler3.txt"
#format and write to file
fe21 = format_list(e21)
fe22 = format_list(e22)
fe23 = format_list(e23)
write_tabular1(fe21, filename1)
write_tabular1(fe22, filename2)
write_tabular1(fe23, filename3)

e23[2]

reload(plts)
# plot def plot_list(pt_list, startx, endx, starty, endy):
plts.plot_list3(f2_sol, e23, e23, e23, 0, 1.2, 0, 2, "2nd Order Euler's Method with steps {0.2, 0.1, 0.05}")

# plot relative error
plts.plot_err( e23, e23, e23, 0, 0 , 0, 0, "Relative error for 2nd Order Euler's Method with steps {0.05}")


#------ Writing tables to Latex

def format_list(multi_list):
    result = []
    for nlist in multi_list:
        result.append(['%.4f' % item for item in nlist])
    return result

#myFormattedList = [ '%.2f' % elem for elem in myList ]


def write_tabular1(list1, filename):
    ff = open(filename, 'w')
    header0 = r'\item[] \textbf{ Table for h = 0.1 }'
    header1 = r'\begin{tabular}{| l | c | r | } \hline'
    header2 = r' & \multicolumn{2}{|c|}{Eulers} \\ \hline'
    header3 = r'$t_0$ & $x_0$ & $error$ \\'

    footer1 = r'\end{tabular}'
    ff.write(header0)
    ff.write('\n')
    ff.write(header1)
    ff.write('\n')
    ff.write(header2)
    ff.write('\n')
    ff.write(header3)
    ff.write('\n')
    for i in range(len(list1[0])):
        line = str(list1[0][i]) + ' & ' + str(list1[1][i]) + '  & ' + str(list1[2][i])
        ff.write(line) + r'  \\  \hline  '
        ff.write('\n')
    ff.write(footer1)
    ff.close()


def write_tabular(list1, list2, list3, filename):
    ff = open(filename, 'w')
    header0 = r'\item[] \textbf{ Table for h = 0.1 }'
    header1 = r'\begin{tabular}{| l | c | c | c | c | c | c | } \hline'
    header2 = r' & \multicolumn{2}{|c|}{Eulers} & \multicolumn{2}{|c|}{Modified Eulers}  & \multicolumn{2}{|c|}{Runge-Kutta} \\ \hline'
    header3 = r'$t_0$ & $x_0$ & $error$ & $x_0$ & $error$ & $x_0$ & $error$ \\'

    footer1 = r'\end{tabular}'
    ff.write(header0)
    ff.write('\n')
    ff.write(header1)
    ff.write('\n')
    ff.write(header2)
    ff.write('\n')
    ff.write(header3)
    ff.write('\n')
    for i in range(len(list1[0])):
        line = str(list1[0][i]) + ' & ' + str(list1[1][i]) + '  & ' + str(list1[2][i]) + ' & '
        line = line +  str(list2[1][i]) + '  & ' + str(list2[2][i]) + ' &  '
        line = line +  str(list3[1][i]) + '  & ' + str(list3[2][i]) + r'  \\  \hline  '
        ff.write(line)
        ff.write('\n')
    ff.write(footer1)
    ff.close()






#----- Write data to a latex table:


# format then write list
# Write for h = 0.2
fe1 = format_list(e1)
fme1 = format_list(em1)
fr1 = format_list(r1)
write_tabular(fe1, fme1, fr1, filename)

# Write for h = 0.1
fe2 = format_list(e2)
fme2 = format_list(em2)
fr2 = format_list(r2)
write_tabular(fe2, fme2, fr2, filename)

# Write for h = 0.1
fe3 = format_list(e3)
fme3 = format_list(em3)
fr3 = format_list(r3)
write_tabular(fe3, fme3, fr3, filename)




