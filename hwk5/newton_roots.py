from __future__ import division
import plotting as plts
from math import *

__author__ = 'nathanael'

def f1(x):
    return 1/(1 + e**(-2*x)) + cos(x)

def f0(x):
    return 1/(1 + e**(-2*x))

def f02(x):
    -cos(x)

def f1p(x):
    return ((2*e**(-2*x))/(e**(-2*x) + 1)**2) - sin(x)


def f2(x):
    return 9*x**3 - x**2 - 1

def f2p(x):
    return 27*x**2 - 2*x


def newtons(f1, f1p, x0, max_iter, epsilon):
    #x1 = x0
    x1 = -100000
    prev = x0
    iter = 0
    while fabs(x1 - prev ) > epsilon and iter < max_iter:
        #print "x0 := ",  x0
        x1 = x0 - (f1(x0)/f1p(x0))
        print "\$x_",iter,"\$ &= ", x1, " \\ \hline"
        prev = x0
        x0 = x1
        iter += 1
    return (x1, iter)


epsilon = 0.00000005
## find lower root
newtons(f1, f1p, 3, 20, epsilon)
## find upper root
newtons(f1, f1p, 3.3, 20, epsilon)
# problem 4
newtons(f2, f2p, 0.6, 20, epsilon)


# Plotting functions
f1(30)
reload(plts)
title = "Function plot"
label1 = r'$\frac{1}{(1 + e^{(-2x)}} + \cos(x)$'

label01 = r'$\frac{1}{(1 + e^{(-2x)}}'
label02 = r'$-\cos(x)$'
labels = [label01, label02]
funcs = [f0, f02]


label1 = r'$9x^3 - x^2 - 1 $'
label02 = r'$-\cos(x)$'

plts.plot_func(f2, 0, 0.6, label1, title)
plts.plot_funcs(funcs, -4, 4, labels, title)






