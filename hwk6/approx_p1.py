from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

__author__ = 'nathanael'


def mean_power(data, power):
    '''
    Returns mean of the first values in the
    array tuple of taken to the power
    :param data:
    :param power:
    :return:
    '''
    if  power == 0:
        return 1
    else:
        mu = np.mean(data[0]**power)
    return mu


def mean_xy(data, power):
    '''
    Returns mean of the x^n * y
    where
    :param data:
    :param power:
    :return:
    '''
    if power == 0:
        mu =  mu = np.mean(data[1])
    else:
        mu = np.mean((data[0]**power)*data[1])
    return mu


def diagonals(dim):
    '''
    Returns a list of positions corresponding to
    the right slanting (/) diagonals of a matrix
    :param dim:
    :return: list of list of tuples with each ith
    list representing the ith diagonal of a matrix
    of size dim X dim
    '''
    num_diag = 2*(dim -1) + 1
    pair_list = [0]* (num_diag)
    for i in range(dim):
        pairs1 = []
        pairs2 = []
        for j in range(i+1):
            a = i - j
            b = 0 + j
            pair = (a, b )
            pairs1.append(pair)
            if(i < dim -1):
                pairs2.append((a + dim-1 -i, b + dim-1 -i))
                pair_list[num_diag-i-1] = pairs2
        pair_list[i] = pairs1
        # print pairs1, pairs2
    return pair_list



def normal_matrix(data, degree):
    '''
    Return least squares normal matrix for a
    polynomial of the given degree
    :param data:
    :param degree:
    :return: 2d numpy array representation of a matrix
    '''
    mat = np.zeros((degree, degree))
    pos_list = diagonals(degree)
    for i in range(len(pos_list)):
        mu = mean_power(data, i)
        pos = pos_list[i]
        for j in range(len(pos)):
        # print pos[0], pos[1]
            mat[pos[j][0]][pos[j][1]] = mu
    return mat

def b_vector(data, degree):
    mat = np.zeros(degree)
    for i in range(degree):
        mat[i] = mean_xy(data, i)
    return mat

def fit_data(data, degree):
    '''
    Returns a numpy poly1d fit of the data of
    the given degree
    '''
    a = normal_matrix(data, degree)
    b = b_vector(data, degree)
    x = np.linalg.solve(a, b)
    # reverse array
    x = x[::-1]
    p = np.poly1d(x)
    return p



def residual_sum(data, fits):
    '''
    Return list of the sum of squared
    errors for each polynomial fit in the
    list of fits
    :param data:
    :param fits:
    :return: list of sums
    '''
    sums = []
    for fit in fits:
        ys = fit(data[0])
        summed_err = sum((data[1]-ys)**2)
        sums.append(summed_err)
    return sums


def poly_fits(data, degrees):
    '''
    fit multiple degrees and return a list of polynomials
    :param data:
    :param degrees:
    :return:
    '''
    ps = []
    for degree in degrees:
        p = fit_data(data, degree)
        ps.append(p)
    return ps




errs = residual_sum(data, fits); errs
dif = fits[0](data[0]) - data[1]

#fits

#errs[0]
# num = 2
# poly_plot1(fits, data, num)



def poly_plot(fits, data):
    plt.title("Least squares polynomials")

    plt.plot(data[0], data[1], 'o')

    startx = -0.2
    endx = 3.3
    increment = 200
    step = (endx - startx)/increment
    # Compute lists of x and y coordinates for the two graphs
    xs = [startx + k*step for k in range(increment)]

    ys1 = fits[0](xs)
    ys2 = fits[1](xs)
    ys3 = fits[2](xs)
    ys4 = fits[3](xs)

    # plot points
    plt.plot(data[0], data[1], 'co')
    # plot fits
    plt.plot(xs, ys1,'k-', label = "degree 1")
    plt.plot(xs, ys2,'g-', label = "degree 2")
    plt.plot(xs, ys3,'b-', label = "degree 3")
    plt.plot(xs, ys4,'m-', label = "degree 4")

    #plt.plot(pt_list2[0], pt_list2[2], 'g--', label="h = 0.1")
    #plt.plot(pt_list3[0], pt_list3[2], 'b-.', label= "h = 0.05")

    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='white')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.legend(loc='lower right')
    # Display graphs
    plt.show()


def poly_plot1(fit, data, num):
    '''
    Plots single polynomial from list of polynomials
    :param fit:
    :param data:
    :param num: number in list to plot
    :return:
    '''
    plt.title("Least squares polynomials")

    plt.plot(data[0], data[1], 'o')
    startx = -1.5
    endx = 1.5
    increment = 200
    step = (endx - startx)/increment
    # Compute lists of x and y coordinates for the two graphs
    xs = [startx + k*step for k in range(increment)]

    ys1 = fits[num](xs)

    # plot points
    plt.plot(data[0], data[1], 'co')
    # plot fits
    plt.plot(xs, ys1,'k-', label = "degree" + str(num +1))

    #plt.plot(pt_list2[0], pt_list2[2], 'g--', label="h = 0.1")
    #plt.plot(pt_list3[0], pt_list3[2], 'b-.', label= "h = 0.05")

    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.legend(loc='lower right')
    # Display graphs
    plt.show()






## Problem 1 script
# data = np.array([[1,2,3,4,5,6,7],[20, 10, 9, 8, 5, 1, 0]])
data = np.array([[0.0, 0.2, 0.5, 0.7, 1.1, 1.5, 1.9, 2.3, 2.8, 3.1],
                 [102.56, 113.18, 130.11, 142.05, 167.53, 195.14, 224.87, 256.73, 299.50, 326.72] ])

degs = [2, 3, 4, 5]
fits = poly_fits(data, degs)
poly_plot(fits, data)
