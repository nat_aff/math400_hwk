from __future__ import division
from math import *
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate

__author__ = 'nathanael'


__author__ = 'nathanael'

# Some matrix calculations:


def f0(a, b , x, y):
    # return 3*vec[0]**2 - vec[1]**2
    return (y - b*e**(a*x))*(x*e**(a*x))


def f1(vec):
    return (y - b*e**(a*x))*( e**(a*x))



def j11(a, b, x, y):
    return (x**2)*(e**(a*x))*(y  - 2* b*e**(a*x))

def j12(a, b, x, y):
    return -x * e**(2*a*x)

def j21(a, b, x, y):
    return (x*e**(a*x))*(y - 2*b**(a*x))

def j22(a, b, x, y):
    return -e**(2*a*x)


def newton2d(f1s, jfun, x0, max_iter, epsilon):
    #x1 = x0
    x1 = (10000, 0)
    prev = x0
    iter = 0
    while fabs(vec_inf_norm(x1) - vec_inf_norm(prev)) > epsilon and iter < max_iter:
        delta_x0 = delta_x(jfun, funs, x0)
        x1 = (x0[0] + delta_x0[0], x0[1] + delta_x0[1])
        print "xn+1 := ", x1
        prev = x0
        x0 = x1
        iter += 1
    return (x1, iter)



def delta_x(jfun, funs, x0):
    mat = np.matrix([[jfun[0](x0), jfun[1](x0)],[jfun[2](x0),jfun[3](x0)]])
    inv = mat.getI()
    fxs = np.array([[funs[0](x0)], [funs[1](x0)]])
    fxs = fxs *-1
    #print "inv mat: ", mat,  inv, fxs
    result = inv*fxs
    result = (result.item(0), result.item(1))
    return result



def vec_inf_norm(int_list):
    '''`
    Get
    :param int_list:
    :return:
    '''
    list_max = max(max(int_list), min(int_list), key=abs)
    return abs(list_max)



def func_res(x):
    return 1.479542794*e**(-1.067830246*x)



def plot_exp( limits):
    pad = .5
    plt.title("Approximation using exponential basis")
    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.axis([pad, 5+pad, -2.5,  5], 'equal')
    startx = limits[0]
    endx = limits[1]
    increment = 200
    step = (endx - startx)/increment
    # Compute lists of x and y coordinates for the two graphs
    xs = [startx + k*step for k in range(increment)]
    # create list for plotting
    ys = [func_res(x) for x in xs]
    x = [0.0100, 0.998, 2.1203, 3.0023, 3.9892, 5.017]
    y = [0.96311, 0.5221, 0.2330, 0.1248, 0.0107, 0.0065]    # plot step
    plt.plot(x,y, 'ro')
    # plot fits
    plt.plot(xs, ys,'g-', label = r'$ f(x) = 1.48*e^{-1.068x}$')
    plt.legend(loc='lower right')
    plt.show()



#-------



x = np.array([0.0100, 0.998, 2.1203, 3.0023, 3.9892, 5.017])
y = ([0.96311, 0.5221, 0.2330, 0.1248, 0.0107, 0.0065])
data = (x,y)

y_est = np.array([func_res(x) for x in data[0]])

err_sum =  summed_err = sum((data[1] - y_est)**2)
err_sum

#------- Running problem 4



plot_exp( (-0, 5))

