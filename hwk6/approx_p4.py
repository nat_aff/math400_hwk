from __future__ import division
from math import *
# import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate

__author__ = 'nathanael'


def cos_basis(k):
    '''
    Return the kth basis function - cos (k*pi*x)
    :param k:
    :return:
    '''
    return lambda x: cos(pi*k*x)


def trig_basis(x, coeff):
    '''
    Evaluate x for the given set of coefficients
    :param x:
    :param coeff:
    :return:
    '''
    res = 0.5
    for i in range(1, len(coeff)):
        res = res + coeff[i]*cos_basis(i)(x)
    return res


def eval_coeff(max_val):
    '''
    Evaluates the coefficients for the cos (k*pi) basis
    where a_0 = 2* integral (0 - 0.5) cos (k*pi),
     a_k = 2* integral (0 - 0.5) cos (k*pi)
    :param max_val:
    :return:
    '''
    coeffs = [2*integrate.quad(lambda x : cos_basis(i)(x), 0, 0.5)[0] for i in range( max_val)]
    # correcting first value since the denominator is 2 for a_k, k = 0
    coeffs[0] = coeffs[0]/2
    return coeffs


def y_coords(coeffs, max, xs):
    ys = [trig_basis(x, coeffs[0:max]) for x in xs]
    return ys


def plot_step2(trig_basis, coeffs, limits):
    pad = .5
    plt.title("Approximation using trigonometric basis functions")
    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.axis([-1-pad, 1+pad, -2.5, 2.5], 'equal')
    startx = limits[0]
    endx = limits[1]
    increment = 200
    step = (endx - startx)/increment
    # Compute lists of x and y coordinates for the two graphs
    xs = [startx + k*step for k in range(increment)]
    # create list for plotting
    ys0 = y_coords(coeffs, 1, xs)
    ys1 = y_coords(coeffs, 2, xs)
    ys2 = y_coords(coeffs, 4, xs)
    ys3 = y_coords(coeffs, 6, xs)
    ys10 = y_coords(coeffs, 21, xs)
    x = [-1, -0.5, -0.5, 0.5, 0.5, 1]
    y = [0, 0, 1, 1, 0, 0]    # plot step
    plt.plot(x, y)
    # plot fits
    plt.plot(xs, ys0, 'g-', label="degree 0")
    plt.plot(xs, ys1, 'b-', label="degree 1")
    plt.plot(xs, ys2, 'r-', label="degree 3")
    plt.plot(xs, ys3, 'k-', label="degree 5")
    plt.plot(xs, ys10, 'm-', label="degree 21")
    plt.legend(loc='lower right')
    plt.show()


# Running problem 4
coeffs = eval_coeff(21)
plot_step2(trig_basis, coeffs, (-2, 2))

