from __future__ import division
from math import *
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate

__author__ = 'nathanael'



def basis(index, x):
    '''
    Legendre polynomials of degree 0-4
    :param index:
    :param x:
    :return:
    '''
    if index == 0:
        return 1
    elif index == 1:
        return x
    elif index == 2:
        return ((1./2.)*(3.*x**2-1))
    elif index == 3:
        return ((1./2.)*(5.*x**3-3*x))
    elif index == 4:
        return ((1./8.)*(35.*x**4-30.*x**2+3))
    elif index == 5:
        return ((1./8.)*(63.*x**5-70.*x**3+15*x))


def eval_fit(coeffs, degree, x):
    '''
    Evaluate the legendre polynomial of the degree
    :param coeffs:
    :param degree:
    :param x:
    :return:
    '''
    res = 0
    for k in range (degree + 1):
        res = res + coeffs[k]*basis(k, x)
    return res



def y_coords(coeffs, degree, xs):
    '''
    Evaluate a the Legendre polynomial fit of a given
    degree and set of coefficients.
    :param coeffs:
    :param degree:
    :param xs:
    :return:
    '''
    ys =[eval_fit(coeffs,degree, x) for x in xs]
    return ys


def eval_coeffs(max_deg):
    '''
    Evaluate the coefficients of the Legendre polynomial
    up to the given max degre
    :param max_deg: maximum degree coefficient to evaluate.
    Value must be supported in basis function
    :return:
    '''
    coeffs = []
    for k in range(max_deg):
        num = integrate.quad(lambda x: basis(k,x), -0.5, 0.5)[0]
        denom = integrate.quad(lambda x: basis(k,x)*basis(k,x), -1, 1)[0]
        coeffs.append(num/denom)
    return coeffs




def plot_step2(coeffs, limits):
    pad = .5
    plt.title("Approximation using Legendre polynomial basis")
    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.axis([-1-pad, 1+pad, -2.5, 2.5], 'equal')
    startx = limits[0]
    endx = limits[1]
    increment = 200
    step = (endx - startx)/increment
    # Compute lists of x and y coordinates for the two graphs
    xs = [startx + k*step for k in range(increment)]
    # create list for plotting

    ys0 = y_coords(coeffs, 0, xs)
    ys1 = y_coords(coeffs, 2, xs)
    # ys2 = y_coords(coeffs, 3, xs)
    ys3 = y_coords(coeffs, 4, xs)
    # ys4 = y_coords(coeffs, 499, xs)


    x = [-1, -0.5, -0.5, 0.5, 0.5, 1]
    y = [0, 0, 1, 1, 0, 0]    # plot step
    plt.plot(x,y)
    # plot fits
    plt.plot(xs, ys0,'g-', label = "degree 0")
    plt.plot(xs, ys1,'b-', label = "degree 2")
    # plt.plot(xs, ys2,'r-', label = "degree 3")
    plt.plot(xs, ys3,'k-', label = "degree 4")
    # plt.plot(xs, ys10,'m-', label = "degree 499")
    plt.legend(loc='lower right')
    plt.show()



#------- Running problem 4
coeffs = eval_coeffs(5); coeffs
plot_step2(coeffs, (-2, 2))




