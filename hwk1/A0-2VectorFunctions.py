
# coding: utf-8

# In[7]:

m400group = 1   # change this to your group number

m400names = ["Nathanael Aff", "Eva Alvarez", "Dominic Brooks", "Thomas Cassady"] # change this for your names

def printNames():
    print("vecFunctions.py for group {0}".format(m400group)),
    for name in m400names:
        print("{0}, ".format(name)),
    print

printNames()


# In[13]:

s = 20
v = [1,2,3]

len(v)


# In[12]:

s = 20 
V = [1,2,3,4,5,6]
T = [6,5,4,3,2,1]
scalarMult(s,V)


addVectors(V,T)
dot(V,T) == 6 + 10 + 12 + 12 + 10 + 6


# In[17]:

s= 1
vec = [1,2,3]
print ("string",  vec)


# In[14]:

def scalarMult(s,V):
    R = []
    for i in range(len(V)):
        R.append(s*V[i])
    return R


# In[25]:

s = 20
V = [1,2,3]
scalarMult(s,V)


# In[37]:

def addVectors(S,T):
    R = []
    if len(S) != len(T):
        print "Vectors not of equal length"
    else:
        for i in range(len(S)):
            R.append(S[i] + T[i] )
    return R


# In[21]:

def dot(S,T):
    R = 0
    if len(S) != len(T):
        print "Vectors not of equal length"
    else:
        for i in range(len(S)):
           R += S[i] * T[i] 
    return R


# In[20]:

def testVecFunctions():
    "test vector function"
    S = [1,2,4,5,9]
    T = [3,7,-5,2,12]
    print("{0} dot {1} = {2}".format(S,T, dot(S,T)))
    print("{0} + {1} = {2}".format(S,T, addVectors(S,T)))
    print("scalarMult({0},{1})= {2}".format(3,T, scalarMult(3,T)))
    


# In[18]:

testVecFunctions()


# In[ ]:



