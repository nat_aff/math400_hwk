
# coding: utf-8

## Matrix Functions Homework

# This is a skeleton for the matrix part of first Math 400 assignment.
# It contains some sample code and stubs for the six matrix functions you need to write.
# 
# Math 400 2/4/15

# In[2]:

m400group = 1   # change this to your group number

# change this for your names

m400names = ["Nathanael Aff","Eva Alvarez", "Dominic Brooks", "Thomas Cassady"] 

def printNames():
    print("matrixFunctions.py for group {0}".format(m400group)),
    for name in m400names:
        print("{0}, ".format(name)),
    print

printNames()


# ### Vector Functions
# 
# Copy these three functions from your finished vectorFunctions.py file
# 
# ~~~~ python
# def scalarMult(s,V):
#     "return vector sV"
#     return("write scalarMult()")    
# 
# 
# def addVectors(S,T):
#     "return S+T"
#     return("write addVectors()")
# 
# 
# def dot(S,T):
#     "return dot product of two vectors"
#     return("write dot()")
# 
# ~~~~
# 

# In[ ]:

#def scalarMult(s,V):


# In[ ]:

#def addVectors(S,T):


# In[1]:

#def dot(S,T):


# In[65]:

#Matrix test
A = [[1,2],[3,4],[5,6]]
A
n,m = shape(A)
n,m


# In[86]:

showMatrix(A)
rows(A)
cols(A)
# thing in (i[1] for i in mylist):
for col in (i[0] for i in A):
    print col
getCol(A, 1)
#b = transpose(A)
for i in range(len(A)):
    getRow(A,i)
    
s1 = 2
B = scalarMultMatrix(s1, A)


# In[85]:

m,n = shape(A)
m,n
p,q = shape(D)
p,q


# In[67]:

C = addMatrices(A,B)
C


# In[72]:

A
B


# In[ ]:




# In[93]:

v = [1,2,3]
x = transposeVector(v)
x = getCol(A, 1)
print A
x
D = transpose(B)
D
Z = multiplyMat(A,D)
Z


# ## Useful Matrix Functions
# 
# Here are three matrix functions that might be useful.

# In[16]:

mat1 = [[1,2,3],[2,3,4]]
showMatrix(mat1)
cols(mat1)
rows(mat1)
x,y = 1,2
shape(mat1)
getCol(mat1,1)


# In[24]:


def showMatrix(mat):
    for row in mat:
        print(row)


def rows(mat):
    "return number of rows"
    return(len(mat))

def cols(mat):
    "return number of cols"
    return(len(mat[0]))


 


# ## Functions for you to finish

# In[23]:

def getCol(mat, col):
    "return column col from matrix mat"
    r_col = []
    if col >= len(mat[0]) or col < 0:
        print "column number is not valid"
    else: 
        for col_val in (row[col] for row in mat):
            r_col.append(col_val)
        return r_col


# In[9]:

def transpose(mat):
    m,n = shape(mat)
    "return transpose of mat"
    ret_mat = []
    for col in range(len(mat[0])):
        #print col
        temp = []
        temp = getCol(mat,col)
        ret_mat.append(temp)
    return ret_mat


# In[25]:

def getRow(mat, row):
    "return row row from matrix mat"
    if row < 0 or row > len(mat):
        print "invalid row number"
    else:
        return mat[row]


# In[27]:

def scalarMultMatrix(s,mat):
    "multiply a scalar times a matrix"
    ret_Matrix = []
    for i in range(len(mat)):
        V = getRow(mat, i)
        R = []
        for i in range(len(V)):
                R.append(s*V[i])
        ret_Matrix.append(R)
    return ret_Matrix


# In[28]:

def addMatrices(A,B):
    ret_matrix = []
    m,n = shape(A)
    p,q = shape(B)
    #Test if matrices are the same shape
    if m != p or n != q:
        print "Matrices are not the same shape"
    else:
        for row in range(m):
            temp = []
            for col in range(n):
                    temp.append(A[row][col]+ B[row][col])
            #print temp
            ret_matrix.append(temp)
    return ret_matrix


# In[29]:

def multiplyMat(mat1,mat2):
    "multiply two matrices"
    m,n = shape(mat1)
    p,q = shape(mat2)
    ret_matrix = []
    #Test if matrices are compatible
    if n != p:
        print n,p
        print "Matrices cannot be multiplied"
    else:
        for i in range(m):
            for j in range(q):
                temp = []
                for k in range(p):
                    temp.append(dot(getRow(mat1,i), getCol(mat2,j)))
             #   print temp
            ret_matrix.append(temp)
    return ret_matrix


# In[30]:

def dot(S,T):
    R = 0
    if len(S) != len(T):
        print "Vectors not of equal length"
    else:
        for i in range(len(S)):
           R += S[i] * T[i] 
    return R


# ### Test Area
# 

# In[32]:


######  Initial tests

A= [[4,-2,1,11],
    [-2,4,-2,-16],
    [1,-2,4,17]]

Ae= [[4,-2,1],
    [-2,4,-2],
    [1,-2,4]]


Bv=[11,-16,17]

Bm=[[11,-16,17]]

C=[2,3,5]


def testMatrix():
    print("A")
    showMatrix(A)
    print("Bm")
    showMatrix(Bm)
    print("Ae")
    showMatrix(Ae)
    print("multiplyMat(Ae,A)")
    showMatrix(multiplyMat(Ae,A))
    print("scalarMultMatrix(2,A))")
    showMatrix(scalarMultMatrix(2,A))
    print("addMatrices(A,A)")
    showMatrix(addMatrices(A,A))
    print("transpose(A)")
    showMatrix(transpose(A))




# Remove the `#` to uncomment functions to try out your three functions when you are ready.

# In[33]:

testMatrix()


# In[ ]:



