from __future__ import division
import integration as int
from math import *
import matplotlib.pyplot as plt
from decimal import *
__author__ = 'nathanael'

# f = lambda x: x > 100 and 'big' or 'small

def equals_f(f1, f2):
    epsilon = 0.00001
    if f1 - epsilon <= f2 <= f1 + epsilon:
        print "True"
    else:
        print "False"


'''
For each of the functions estimate the integral to
seven decimal places. Use
1. Gaussian quadrature
2. Trapezoidal
3. Romberg
State how many points were needed to achieve the accuracy
Provide a graph of each function over the interval
'''


def func1(x):
    return sin(pi * x)

def func2(x):
    return exp(-(x**2)/2)

def func3(x):
    if x == 0:
        return 1
    else:
        return sin(2*pi*x)/(2*pi*x)

# function on the interval (-1, 1)
# f(x) -> (b-a)/2 * f( (b-a)/2 u + (b+a)/2) du
def ufunc1(x):
    return  sin((pi/4) *x + pi/4)

def ufunc2(x):
    return exp(-((1/2 *x + 3/2)**2)/2)

def ufunc3(x):
    return e
# check evaluation at endpoints
equals_f(ufunc1(1), func1(1/2))
equals_f(ufunc1(-1), func1(0))
equals_f(ufunc2(1), func2(2))
equals_f(ufunc2(-1), func2(1))

#misses 1.6
def func4(x):
    vals = [
    0.24197072,
    0.21785218,
    0.19418605,
    0.17136859,
    0.14972747,
    0.12951760,
    0.11092083,
    0.09404908,
    0.07895016,
    ]
    index = trunc((x*10 - 1.0*10) )
    print index
    return vals[index]



#-------- plot functions
def plot_function(func, startx, endx, starty, endy, lab, increment = 20):
    delta_theta = pi/increment
    # Compute lists of x and y coordinates for the two graphs
    thetas = [-pi+k*delta_theta for k in range(3*increment+1)]
    fx1 = [func(t) for t in thetas]

    # Plot the curves and (border) axes
    plt.plot(thetas, fx1, color='red', label = lab)
    plt.axis([startx, endx, starty, endy], 3/4)
    # draw axes centered at the origin
    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.legend(loc='lower right')

    # Display graphs
    plt.show()


def plot_num(x):
    f1_str = r'$sin \pi dx$'
    f2_str = r'$e^{(-x^2/2)}$'
    f3_str = r'$Sinc(2*\pi*x)$'

    # func1 on interval (0, 1/2)
    # func2 on interval (1, 2)
    # func3 on interval (-1, 1)
    plot_function(func1, -1, 1, -1.5, 1.5, f1_str, 1000  )
    plot_function(func2, -1, 2, -1, 2, f2_str, 20)
    plot_function(func3, -1.5, 1.5, -1.5, 1.5, f3_str, 200)
    return

#----------------

# Problem
reload(int)
# a) sin(pix)
result = int.iterative_int(func1, (0, .5), 2000, .5, True)
# iterations 4
result
result = int.iterative_nox(func1, (0, .5), 2000, .5, True)
result
gresult = int.gauss_quad(ufunc1, 8)


# b guass
result = int.iterative_int(func2, (1, 2), 2000, .5, True)
result
result = int.iterative_nox(func2, (1, 2), 2000, 1, True)
result
gresult = int.gauss_quad(ufunc2, 8)


# c) Sinc(2pi x) estimate:0.4514116667901403133978501889534
result = int.iterative_int(func3, (-1, 1), 2000, 1, True)
result
result = int.iterative_nox(func3, (-1, 1), 2000, 1, True)
result
gresult = int.gauss_quad(func3, 10)
gresult - 0.45141166679014031


# problem 3 -- list centered difference results for h = 10^-1 to 10^-20
# include column of relative error for each estimate
def problem_3():
    true_val = -0.525435538391959354199974
    hs = []
    for i in range(1,21):
        hs.append(10**-i)
    hs
    tbl = table_vals(1.4, hs, func2, true_val)
    return tbl

def table_vals(x, hs, func, val):
    table = []
    for h in hs:
        result = int.central_diff(func, x, h)
        err = abs(result - val)/abs(val)
        table.append((result, err))
    return table

#problem_3()


# problem 4 :
# estimate using expanded central difference and integration
#result = int.romburg_int(func4, (1, 1.8), (3,3), .4, True)
#result


