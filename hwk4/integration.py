from __future__ import division
from math import *
__author__ = 'nathanael'


def partition(a, b, num):
    steps = []
    steps.append(a)
    increment = (b-a)/num
    for i in range(num):
        a = a + increment
        steps.append(a)
    return steps

def equals_f(f1, f2):
    epsilon = 0.0000005
    if f1 - epsilon <= f2 <= f1 + epsilon:
        return True
    else:
        return False


#example function
def func1(x):
    return x**2

def func2(x):
    return x**(3/2)

def func3(x):
    return -exp(-(x**2))

def test_diff():
    r1 = central_diff(func3, .75, .5)
    if r1 <  0.728 or r1  > 0.73:
        print "incorrect result:", r1
    else:
        print "correct result:", r1

def central_diff(func, x, h):
    fpr = (func(x + h) - func(x - h))/(2*h)
    return fpr


# int_a^b f(x) = h/2(f(x) + 1/2f(x) + 1/2 f(x) + 1/2 f(b))
def trapezoidal(func, a, b, h):
    tot = (func(a) + func(b)) * 1/2
    steps = partition(a,b, trunc((b-a)/h))
    for i in range(1, len(steps)-1):
        res = func(steps[i])
        tot += res
    tot = tot*h
    return tot


# Test trapezoidal -- Results not updated
def test_trap():
    r1 =  trapezoidal(func1, 1, 3, 1)
    if 3 == r1:
        print "Trap1 correct, result:", r1
    else:
        print "Trap1 incorrect!, result:", r1
    r2 = trapezoidal(func1, 0, 3, 1)
    if 2.75 == r2:
        print "Trap2 correct, result:", r2
    else:
        print "Trap2 incorrect, result:", r2

#test_trap()



def romburg_int(func, interval, iteration, h0, integrate=True):
    '''
    formula:
    T(h/2)_{j-1} + T_{j-1} (h/2) - T_{j-1}/ (2^{(j-1)} - 1)
    :param func: the function to integrate
    :param interval: the interval (a,b) to integrate over
    :param iteration: a tuple(i,j) representing the i,jth
    iteration of the Romburg integration
    :return:
    '''
    # ij
    table = []

    for i in range(iteration[0]):
        h = h0/2**i
        if integrate:
            t1 = trapezoidal(func, interval[0], interval[1], h)
        else:
            t1 = central_diff(func, interval[0], h)
        table.append([t1])
    # i corresponds to j in handout.  row/column = ij
    for i in range(1, iteration[1]):
        for j in range(1, i+1):
            t1 = table[i-1][j-1]
            t2 = table[i][j-1]
            th = t2 + (t2 - t1)/(2**(2*(j))-1)
            table[i].append(th)
    return table



def iterative_int(func, interval, max_iter, h0, integrate=True):
    '''
    formula:
    T(h/2)_{j-1} + T_{j-1} (h/2) - T_{j-1}/ (2^{(j-1)} - 1)
    :param func: the function to integrate
    :param interval: the interval (a,b) to integrate over
    iteration of the Romburg integration
    :return:
    '''
    epsilon = 0.00000005
    table = []
    curr = 1; t2 = 0; i = 1;h = h0
    if integrate:
        t1 = trapezoidal(func, interval[0], interval[1], h0)
    else:
        t1 = central_diff(func, interval[0], h0)
    table.append([t1])
    while curr-t2 > epsilon or curr-t2 < -epsilon and i < max_iter:
            h = h0/2**i
            if integrate:
                t1 = trapezoidal(func, interval[0], interval[1], h)
            else:
                t1 = central_diff(func, interval[0], h)
            table.append([t1])
            for j in range(1, i+1):
                t1 = table[i-1][j-1]
                t2 = table[i][j-1]
                #print "t1, t2 : ", t1, t2
                curr = t2 + (t2 - t1)/(2**(2*(j))-1)
                table[i].append(curr)
            i +=1
    pts = calc_points(interval, h)
    print "current iteration:", i
    print "current diference: ", curr- t2
    print "points used: ", pts
    return table



def iterative_nox(func, interval, max_iter, h0, integrate=True):
    '''
    formula:
    T(h/2)_{j-1} + T_{j-1} (h/2) - T_{j-1}/ (2^{(j-1)} - 1)
    :param func: the function to integrate
    :param interval: the interval (a,b) to integrate over
    iteration of the Romburg integration
    :return:
    '''
    epsilon = 0.00000005
    t2 = 0; t1 = -20000; i = 1;h = h0
    if integrate:
        t2 = trapezoidal(func, interval[0], interval[1], h0)
    else:
        t2 = central_diff(func, interval[0], h0)
    while t2 -t1 > epsilon or t2-t1 < -epsilon and i < max_iter:
        t1 = t2
        h = h0/2**i
        if integrate:
            t2 = trapezoidal(func, interval[0], interval[1], h)
        else:
            t2 = central_diff(func, interval[0], h)
        i += 1
    pts = calc_points(interval, h)
    print "current iteration:", i
    print "current difference: ", t2 - t1
    print "points used: ", pts
    return (t2, t1)


#-------- Gaussian Quadrature

# weights (key : (x_i, w_i))
weights = {2: ([0.5773502691896257], [1.0000000000000]),
           3:([0.00000000000000000, 0.774596669241483377],
              [0.888888888888888888, 0.55555555555555555 ]),
           4:([0.33998104358485626,0.8611363115940525752],
              [0.6521451548625461, 0.347854845137453857 ]),
           5:([0.00000000000000000, 0.538469310105683091, 0.906179845938663992],
              [0.5688888888888888888, 0.478628670499366468, 0.236926885056189087  ]),
           6:([0.238619186083196908, 0.661209386466264513, 0.932469514203152027],
              [0.46791393457269104, 0.3607615730481386, 0.17132449237917034 ]),
           7:([0.000000000000000, 0.4058451513773971, 0.741531185599394439,
               0.94910791234275852],
              [0.4179591836734693877, 0.3818300505051189,
               0.279705391489276667, 0.12948496616886969327 ]),
           8:([0.183434642495649804,0.525532409916328985,
               0.796666477413626739, 0.960289856497536231],
              [ 0.3626837833783619, 0.313706645877887287,
                 0.222381034453374, 0.1012285362903762591]),
           9:([0.0000000000000, 0.3242534234038089,
               0.6133714327005903,0.836031107326635, 0.96816023950762],
              [0.3302393550012597, 0.312347077040,
               0.180648160694857, 0.260610696402935, 0.0812743883615]),
           10:([0.148874338981631, 0.43339539412924,
                0.6794095682990, 0.86506336668898,0.97390652851717],
               [ 0.2955242247147, 0.269266719309996,
                 0.2190863625159, 0.14945134915058,
                 0.06667134430868]),
           12:([0.125233408511468, 0.36783149899818,
                0.58731795428661744, 0.76990267419430,
                0.9041172563704, 0.9815606342467],
               [0.24914704581340278, 0.2334925365383,
                0.203167426723065, 0.16007832854334,
                0.106939325995318, 0.047175336386511])}


def gauss_quad(func, max):
    #integral (-1,1) fx = sum(to n) w_i f(x_i)
    n = 2
    t1 = -100000
    print "gauss n", n
    t2 = gauss_sum(func, n)
    while n < max and not equals_f(t1,t2):
        n += 1;t1 = t2
        t2 = gauss_sum(func, n)
    print "current iteration:", n
    print "current difference: ", t2 - t1
    return t2


def gauss_sum(func, n):
    tot = 0
    for i in range(len(weights[n][0])):
        x = weights[n][0][i]
        tot += func(x)*weights[n][1][i]
        if not equals_f(x, 0):
            tot += func(-x)*weights[n][1][i]
    return tot

def func4(x):
    return (x+2)**(3/2)

def calc_points(interval, h):
    return (interval[1]-interval[0])/h + 1

#test gauss sum
#gauss_sum(func4, 5)
#gauss_quad(func4, 7)

#calc_points((0,2), 0.25)

#Example derivative from handout: results:
#0.729802
#0.821843 0.852523
#0.846363 0.854536 0.854670
#0.852590 0.854666 0.854674 0.854674

#romberg_int(func3, (.75, .75), (4,4), .5, False)
#iterative_int(func3,(.75, .75), 500, .5, False )


# Integration Example from handout
# 5.858234
# 5.841100 5.835388
# 5.836813 5.835384 5.835384
# 5.835740 5.835382 5.835382 5.835382

#romberg_int(func2, (1,3), (3,3), .5)
#iterative_int(func2, (1,3), 500, .5)
#iterative_nox(func2, (1,3), 500, .5)
#calc_points((0,1), 1/2**13)



# #tests
# test_trap()
# test_diff()
# # test Romburg
# x = romberg_int(func1, (0,2), (2,1))

