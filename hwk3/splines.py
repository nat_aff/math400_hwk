from __future__ import division

import polynomials as ply

__author__ = 'nathanael'


''' TODO: update with formula from
    notes : interior vs. end points
'''
def linear_poly(data, i):
    # x - xi
    # y1 - y2/ x1 - x0
    coeff = (data[i+1][1] - data[i][1])/(data[i+1][0] - data[i][0])
    b = data[i][1]- (data[i][0]*coeff)
    result = {1:coeff, 0:b}
    return result


def linear_list(data):
    poly_list = []
    for i in range(len(data)-1):
        p = linear_poly(data, i)
        poly_list.append(p)
    return poly_list

#------------------------------
# Linear spline second version
#------------------------------

def linear_list1(nodes):
    poly_list = []
    for i in range(len(nodes)):
        if i == 0:
            poly = lin_theta1(nodes, i)
        elif i == len(nodes)-1:
            poly = lin_theta2(nodes, i)
        else:
            poly = lin_thetas12(nodes, i)
        # y_i * Psi_i
        poly = ply.scalar_mult(nodes[i][1], poly)
        ply.print_poly(poly)
        poly_list.append(poly)
    return poly_list


def lin_thetas12(nodes, i):
    theta1 = lin_theta1(nodes, i)
    theta2 = lin_theta2(nodes, i)
    thetas12 = ply.add(theta1, theta2)
    return thetas12


def lin_theta1(data, i):
    # 1- x where x = theta1_arg
    arg = theta1_arg(data, i)
    arg = ply.scalar_mult(-1, arg)
    theta1 = ply.add({0:1}, arg)
    return theta1

def lin_theta2(data, i):
    #theta2 = x
    arg = theta2_arg(data, i)
    return arg


#------- test piece-wise linear
nodes = [(1,2,3),(2,3,2),(4, -5, -2)]


# reload(pltng)
# lin_list = linear_list(nodes)
# pltng.plot_piecewise_poly2(lin_list, nodes, -10, 10, 0.01)
# # pltng.plot_piecewise_poly(lin2, nodes, -10, 10, 0.01)
#
# lin_list

# pltng.plot_poly(p2, 0, 3, 0.2, 'hi')



#---------------------------
# Cubic spline interpolation
#---------------------------

def cubic_interpolant(data):
    result = []
    for i in range(len(data)):
        if i == 0:
            psi = cubic_theta1(data, 0)
            phi = phi1(data, 0)
        elif i == len(data)-1:
            psi = cubic_theta1(data, i - 1)
            phi = phi1(data, i-1)
        else:
            psi = psi_interior(data, i)
            phi = phi_interior(data, i)
        psi = ply.scalar_mult(data[i][1], psi)
        phi = ply.scalar_mult(data[i][2], phi)
        poly = ply.add(psi, phi)
        result.append(poly)
    return result

def psi_interior(data,i):
    p1 = cubic_theta1(data, i)
    p2 = cubic_theta2(data, i)
    result = ply.add(p1, p2)
    return result

def phi_interior(data, i):
    p1 = phi1(data,i)
    p2 = phi2(data,i)
    result = ply.add(p1,p2)
    return result

def cubic_theta2(data, i):
    theta = {2:3, 3:-2}
    p1 = theta2_arg(data, i)
    result = compose(theta, p1)
    return result

def phi2(data, i):
    scalar = data[i][0] - data[i-1][0]
    phi = {3:1, 1:-2}
    p2 = theta2_arg(data, i)
    result = compose(phi, p2)
    result = ply.scalar_mult(scalar, result)
    return result

def phi1(data, i):
    scalar = data[i+1][0] - data[i][0]
    phi = {3:1, 2:-2, 1:1}
    p2 = theta1_arg(data, i)
    result = compose(phi, p2)
    result = ply.scalar_mult(scalar, result)
    return result

def cubic_theta1(data, i):
    theta = {3:2, 2:-3, 0:1}
    p1 = theta1_arg(data, i)
    result = compose(theta, p1)
    return result

def theta1_arg(data, i):
    # (x - xi)/(xi+1 - xi) = x - y/  z - y
    y = data[i][0]
    z = data[i+1][0]
    a = 1/(z-y)
    b = -y/(z-y)
    # print " a, b", a, b
    p1 = {1: a, 0: b}
    return p1


def theta2_arg(data, i):
    i = i-1
    p1 = theta1_arg(data, i)
    return p1


def compose(left, right):
    # for each term handle powers then coefficients
    poly_terms = []
    for power, coeff in left.iteritems():
        print "power = ", power, coeff
        result = ply.poly_power(right, power)
        result = ply.scalar_mult(coeff, result)
        print "after scalar", result
        poly_terms.append(result)
    result = {0:0}
    print "no terms = ", len(poly_terms)
    for poly in poly_terms:
        result = ply.add(result, poly)
    return result


#---
# tests



#-----------------
# Test thetas
#-----------------
# import plotting as pltng
# data =[(1,2,3),(2,3,2),(4, -5, -2)]
# nodes = [0, 1/6, 1/3, 1/2, 7/12, 2/3, 3/4, 5/6, 11/12]
# data = p3_data(nodes)
#
#
# poly_list = cubic_interpolant(data)
#
# poly2 = (poly_list[1], poly_list[2], poly_list[3], poly_list[4])
#
# pltng.plot_piecewise_poly2(poly2, p3_func, data, -0.5, 10, -2, 4, 0.01, 1000, "Piecewise linear interpolant")

# # poly
# # pts = ply.drange(-2,5, 0.2)
# points = ply.eval_poly_points(poly, pts)

#
# ply.print_poly(poly)
# xs = [x for [x, y, z] in data]
# ys = [y for [x, y, z] in data]
# pts = (xs, ys)

#ply.plot_poly_points(poly, pts, -10, 10, .2, 'hi' )
#
# plt.plot(pts, points)
# plt.plot(xs, ys, 'ro')
# plt.show()
# #--------------------
# #
# #-----------------
