from __future__ import division
import math

DEBUG = True
using_dict = 1

def tuple_derivative(poly):
    '''
    This version works with a polynomial
    defined as a tuple (coefficient, degree)
    '''
    result = []
    p = []
    for i in range(len(poly)):
        if poly[i][1] != 0:
            print i
            p =[poly[i][1]*poly[i][0], poly[i][1] -1]
            result.append(p)
    return result


def derivative(poly):
    result = {}
    for k,v in poly.iteritems():
        if k != 0:
            result[k-1] = v*k
    return result


def print_poly(p1):
    '''
    Function takes polynomial as dictionary polynomial
    where key = exponent, value = coefficient,
    or tuple with (Coeff, exp)
    '''
    if(using_dict):
        poly = []
        for key, value in p1.iteritems():
            temp = [value,key]
            poly.append(temp)
    else: poly = p1

    for i in range(len(poly)):
        if poly[i][1] != 0 and i != len(poly)-1:
            print("{0:.3f}x^{1} + ".format(poly[i][0], poly[i][1])),
        elif i == len(poly)-1 and poly[i][1] != 0:
            print("{0:.3f}x^{1}".format(poly[i][0], poly[i][1]))
        #needs to be fixed
        elif poly[i][1] == 0 and i != len(poly)-1:
            print("{:.3f} +".format(poly[i][0])),
        elif poly[i][1] == 0:
            print("{:.3f}".format(poly[i][0])),


def poly_tuple_func(poly, x):
    '''
    Return result for a polynomial in
    tuple for evaluted at x
    '''
    px = 0
    for term in poly:
        px += x**term[1] * term[0]
    return px


def add(left, right):
    '''
    Add two polynomials -- dictionary version
    '''
    result = {0:0}
    for k,v in left.iteritems():
        if k in right:
            result[k] = left[k] + right[k]
        else: result[k] = v
    for k,v in right.iteritems():
        if k not in left:
            result[k] = right[k]
    result = reduce(result)
    return result


def scalar_mult(scalar, poly):
    '''
    Multiply a polynomial by a scalar
    '''
    result = {}
    for k,v in poly.iteritems():
        result[k] = v*scalar
    return result


def multiply(left, right):
    '''
    Multiplies two polynomials an
    returns the result
    :param left
    :param right
    :return
    '''
    result = {0:0}
    for k1, v1, in right.iteritems():
        temp = {}
        for k2, v2 in left.iteritems():
            #print "k1+k2", k1, k2
            temp[k1+k2] = v1*v2
        result = add(result, temp)
        if DEBUG: print_poly(result)
        result = reduce(result)
    return result


def eval_poly(poly, x):
    '''
    Evaluates dict polynomial at
    x
    :param poly:
    :param x:
    :return:
    '''
    px = 0
    for k,v in poly.iteritems():
        px += (x**k) * v
    return px

#----------------
# Test basic functions
# #----------------
# poly0 = {0:0}
# poly1 = {1:1}
# poly2 ={0:1, 1:2, 2:3}
# padd1 = add(poly0, poly0)
# padd2 = add(padd1, poly2)
# pmult0 = multiply(poly0, poly1)
# addempty = add(pmult0, poly1)
# poly2scal3 = scalar_mult(3, poly2)

#---------------------------
# lagrangian interpolation:
#---------------------------

def reduce(poly):
    '''
    Remove zero coefficients from dictionary
    polynomial
    :param poly:
    :return:
    '''
    result = {}
    for k,v in poly.iteritems():
        if v != 0:
            result[k] = v
    return result


def lagrange_coeff(data, i, times_yi = False):
    '''
    Calculate Lagrange coefficient
    C_i * y_i
    :param data:
    :param i:
    :return:
    '''
    if DEBUG:
        print "term i = ", i
    xi = data[i][0]
    ci = 1
    for j in range(len(data)):
        if j != i:
            ci = ci*(xi - data[j][0])
    if (times_yi):
        print "times yi"
        ci = data[i][1]/ci
    else:ci = 1/ci
    return ci


def lagrange_pxi(data, i):
    '''
    Calculate the ith Lagrange
    polynomial.
    :param data:
    :param i:
    :return:
    '''
    poly = {}
    count = 0
    for j in range(len(data)):
        if j != i:
            # temp = x - x_j
            temp = {1:1, 0:-data[j][0]}
            if count > 0:
                poly = multiply(poly, temp)
            else: poly = temp
            count += 1
    return poly


def lagrange_poly(data):
    '''
    Calculate full Lagrange polynomial:
    Sum {over i} y_i * C_i * pxi
    where y_i is the yth data point , C_i is
    the lagrange coefficient and pxi is the ith
    lagrange polynomial. (y_0*C_i) together.
    :param data: Input data
    :return: The Lagrange polynomial as a dict
    '''
    result ={}
    for i in range(len(data)):
        ci = lagrange_coeff(data, i)
        pxi = lagrange_pxi(data, i)
        # print "ci:", ci, "pxi:"
        # print_poly(pxi)
        poly = scalar_mult(ci, pxi)
        if DEBUG:
            print " poly before y_0"
            print_poly(result)
        poly = scalar_mult(data[i][1], poly)
        result = add(result, poly)
        if DEBUG:
            print_poly(result)
    return result


def drange(start, end, step):
    iterations = int(math.ceil((end-start)/step)) + 1
    print "iter", iterations
    seq = [start + i*step for i in range(iterations)]
    return seq



def lagrange_term(data, i, times_yi=False):
    '''
    Calculate the ith term of the lagrange polynomial.
    This version used in creating the ith Hermite polynomial
    :param data:
    :param i:
    :return:
    '''
    if DEBUG:
        print "term i = ", i
    ci = lagrange_coeff(data, i, times_yi)
    pxi = lagrange_pxi(data, i)
    result = scalar_mult(ci, pxi)
    if DEBUG:
        print_poly(result)
    return result

#-------------------------------
# Test basic lagrange
#-------------------------------
# homework data :
# data = [(1,2),(2,3),(4,1)]
# poly = lagrange_poly(data)
# poly
#
# poly = {0:0}
# for i in range(len(data)):
#     print i
#     cxi = lagrange_coeff(data, i, False)
#     pxi = lagrange_pxi(data,  i)
#     pxi = scalar_mult(cxi, pxi)
#     yxi = data[i][1]
#     print ("pxi = ", pxi)
#     poly = add(poly, pxi)
#     print "in loop poly"
#     print_poly(poly)
# poly
#
# px0 = lagrange_pxi(data, 0)
# cx0 = lagrange_coeff(data, 0)
# px0 = scalar_mult(cx0, px0)
#
# px1 = lagrange_pxi(data, 1)
# cx1 = lagrange_coeff(data, 1)
# px1 = scalar_mult(cx1, px1)
# cx2 = lagrange_coeff(data, 2)
# px2 = lagrange_pxi(data, 2)
# pxi = scalar_mult(cx2, px2)
# print "p0 =", px0
# pxres = add(px1, px0)
# print "p1 = ", pxres
# pxres = add(pxi, pxres)
# print "p2 = ", pxres
# pxres
#
# DEBUG = True
# pex = lagrange_poly(data)
# print_poly(pex)
#--------------------------------

def eval_poly_points(poly, points):
    '''
    Return a list of values of a polynomial
    evaluated at input list of points
    :param poly: Input polynomial - a dict
    :param points: Input x_i - list
    :return: List of y_i values
    '''
    result =[]
    for i in points:
        result.append(eval_poly(poly, i))
    return result




def point_list(plist, data, step):
    ret_pts = []
    for i in range(len(plist)):
        # Use x_values for range
        pts = drange(data[i][0], data[i+1][0], step)
        fx = eval_pts(plist[i], pts)
        ret_pts.append([pts, fx])
    return ret_pts


def eval_pts(poly, points):
    fx = []
    for pt in points:
        fx.append(eval_poly(poly, pt))
    return fx


#-----------------------------
#  Hermite polynomials
#
# H_2n+1 = sum j f(x_j) h_nj(x) + sum_j f'(xj) \barH_nj(x)
# h_k(x) = [1- 2L'_k(x_k)(x-x_k)][L_k(x_k]^2
# bar_h(x) = (x -x_k)[L_k(x_k)]^2
#------------------------------
def eval_li(data, i):
    li = lagrange_term(data, i)
    fx = eval_poly(li, data[i][0])
    return fx

def hermite_hterm(data, i):
    #hx = y0 Li^2  -  2Li'(x_k)(x-x_k)Li^2 where Li = lagrange term for i
    li = lagrange_term(data, i)
    li_sq = multiply(li, li)
    li_dx = derivative(li)
    li_eval = eval_poly(li_dx, data[i][0])
    # Calculating the left hand side
    # p1 = (x-x_k)
    p1 = {1:1, 0:-data[i][0]}
    p1 = scalar_mult(li_eval, p1)
    p1 = scalar_mult(-2, p1)
    #y_0 * li^2
    hx = add({0:1}, p1)
    hx = multiply(hx, li_sq)
    hx = scalar_mult(data[i][1], hx)
    #print_poly(hx)
    return hx


def hermite_hbar_term(data,i):
    # hbar = (x-x_k)(L_i(x_k)^2
    p1 = {1:1, 0:-data[i][0]}
    #print_poly(p1)
    li = lagrange_term(data, i)
    li_sq = multiply(li, li)
    hbar = multiply(p1, li_sq)
    hbar = scalar_mult(data[i][2], hbar)
    return hbar


def hermite_poly(data):
    result = {0:0}
    for i in range(len(data)):
        term = add(hermite_hterm(data, i), hermite_hbar_term(data, i))
        result = add(result, term)
        if DEBUG:
            print_poly(result)
    return result


def poly_power(poly, power):
    '''
    Calculates the polynomial poly to the
    power
    :param poly:
    :param power:
    :return:
    '''
    if power == 0:
        result = {0: 1}
    elif power == 1:
        result = poly
    else:
        result = multiply(poly, poly)
        for i in range(power-2):
            result = multiply(result, poly)
    return result



# # ----- hermite test
#
# Data from textbook
# -------------------

# data = [(1.3, 0.6200860, -0.5220232), (1.6, 0.4554022, -0.5698959), (1.9, 0.2818186, -0.5811571)]
# h1 = hermite_poly(data)
# eval_poly(h1, 1.5)
#
#
# DEBUG = False
# test_terms(data, results)
#
# def test_terms(data, results):
#     for i in range(len(data)):
#         print "h1"
#         h1 = hermite_hterm(data, i)
#         compare(eval_poly(h1, 1.5), results[i][0])
#         print "Hb"
#         hb = hermite_hbar_term( data, i)
#         compare(eval_poly(hb, 1.5), results[i][1])
#     return
#
# def compare(left, right):
#     epsilon = 0.0001
#     if math.fabs(left - right) > epsilon:
#         print "Incorrect : ", left, " ", right
#     else:
#         print "Correct :" , left, " ", right
#     return

