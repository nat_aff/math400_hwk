"""

This program is a simplified version of a program "sample-plots.py" by 
Bruce Cohen 3/5/12.
Modified 3/17/15 by ds

"""

import matplotlib.pyplot as plt

from math import *


plt.title(r'$5^{th}$ degree Maclaurin polynomial vs $\sin(x)$')
def taylor_sine(x):
    return(x-(x**3)/(2*3) + (x**5)/(2*3*4*5))

taylor_label = r'$x- \frac{x^3}{3!}+\frac{x^5}{5!}$'

n = 36 # number of intervals per unit
delta_theta = pi/n

# Compute lists of x and y coordinates for the two graphs
thetas = [-pi+k*delta_theta for k in range(3*n+1)]
taylor_ys = [taylor_sine(t) for t in thetas]
sin_ys = [sin(t) for t in thetas]

# Plot the curves and (border) axes
plt.plot(thetas, taylor_ys, color='red', label=taylor_label)
plt.plot(thetas, sin_ys, color='blue', label= r'$\sin(x)$')
plt.axis([-pi, 2*pi , -2, 2], 3/4 )

# draw axes centered at the origin
# first a default hline at y=0 that spans the xrange
plt.axhline(y=0, color='black')

# second a default vline at x=0 that spans the yrange
plt.axvline(x=0, color='black')

plt.legend(loc='lower right')

# Display graphs
plt.show()

# This doesn't seem to be working.
plt.savefig("taylor_sin.png")


