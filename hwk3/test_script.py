from __future__ import division
__author__ = 'nathanael'

import math
import polynomials as ply
import splines as spl
import plotting as pltng

#----------------------------
# problem 3 :
# 1. Generate data for f(x) 1.6e^{-2x}sin(3pi x)
#----------------------------

def test_func(x):
    y = math.sin(x)
    return y

def p3_func(x):
    y = 1.6* (pow(math.e, -2*x)) * math.sin(3 * math.pi *x)
    return y

def p3_deriv(x):
    y = pow(math.e, -2*x)*(15.0796 *math.cos(3*math.pi*x) - 3.2*math.sin(3*math.pi*x))
    return y


def p3_data(x_list):
    data = []
    for x in x_list:
        y = p3_func(x)
        ypr = p3_deriv(x)
        pt = (x, y, ypr)
        data.append(pt)
    return data


#-------------------------
# test results
#-------------------------

def check_fit(poly, ptlist):
    correct = 0
    incorrect = 0
    epsilon  = 0.001
    for pt in ptlist:
        y = pt[1]
        fx = ply.eval_poly(poly, pt[0])
        if y + epsilon >= fx >= y-epsilon:
            print pt[0], "evaluates correctly as", fx
            correct += 1
        else:
            print pt[0], "evaluates incorrectly as", fx
            print fx ," should be",  y
            incorrect += 1
    print "Correct: ", correct, "Incorrect", incorrect
#----- generate data

def prob3_tests(nodes):
    data = p3_data(nodes)
    lagrange = ply.lagrange_poly(data)
    check_fit(lagrange, data)
    hermite = ply.hermite_poly(data)
    check_fit(hermite, data)
    #linear_spline = ply.linear_poly(data)

def prob3_plot(poly, nodes):
    data = p3_data(nodes)
    pt_list = pltng.tuples_to_list(data)
    ###------- needs to chec, min, max, etc.



#--------------------------------------
# Problem 3
#-------------------------------------
#ex_nodes = [(1,2, 1),(2,3, 2),(4,1, -2)]
nodes = [0, 1/6, 1/3, 1/2, 7/12, 2/3, 3/4, 5/6, 11/12, 1]
data = p3_data(nodes)
pt_list = pltng.tuples_to_list(data)

poly = ply.lagrange_poly(data)
ply.print_poly(poly)

pltng.plot_poly_points2(poly, p3_func, pt_list, -0.5, 2, -2, 2, 0.001, 1000,"Lagrangian interpolant", "Lagrange")

print "Printing polynomial"
ply.print_poly(poly)


#---- Test hermite polynomial
# ---------------
nodes = [0, 1/6, 1/3, 1/2, 7/12, 2/3, 3/4, 5/6, 11/12, 1]
#nodes2 = [0, 1/4, 2/4, 3/4]

data = p3_data(nodes)
pt_list = pltng.tuples_to_list(data)

hermite = ply.hermite_poly(data)
ply.print_poly(hermite)
check_fit(hermite, data)
pltng.plot_poly_points2(hermite, p3_func, pt_list, -0.5, 2, -2, 1.5, 0.001, 1000,"Hermite polynomial interpolant", "Hermite")


#----------
# Plot point-wise
#-----------
lin_list = spl.linear_list(data)

pltng.plot_piecewise_poly2(lin_list, p3_func, data, -0.5, 1.4, -2, 4, 0.01, 1000, "Piecewise linear interpolant")

print "Printing polynomials"
for p in lin_list:
    ply.print_poly(p)


#---- Testing Cubic Spline
#--------------------------

#----- Problem 4: pi = lagrange_pxi
l6 = ply.lagrange_term(data, 6)
pltng.plot_poly_points2(l6, p3_func, pt_list, -0.5, 1.2, -20, 20, 0.001, 1000, r'$L_6(x)$', r'$L_6$')

p6 = ply.lagrange_pxi(data, 6)
pltng.plot_poly_points2(p6, p3_func, pt_list, -0.5, 1.2, -0.00025, 0.00025, 0.001, 1000, r'$pi_6(x)$', r'$pi_6$')
ply.print_poly(p6)

ci = ply.lagrange_coeff(data, 6)
print "coefficient of L_6 is: ", ci