__author__ = 'nathanael'

"""

This program is a simplified version of a program "sample-plots.py" by
Bruce Cohen 3/5/12.
Modified 3/17/15 by ds

"""

import matplotlib.pyplot as plt

from math import *
import polynomials as ply


def taylor_sine(x):
    return(x-(x**3)/(2*3) + (x**5)/(2*3*4*5))


def sinex(x):
    return sin(x)


def example_plot():
    '''
    This is the example plot of two functions written by
    Bruce Cohen/Sklar
    :return:
    '''
    plt.title(r'$5^{th}$ degree Maclaurin polynomial vs $\sin(x)$')

    taylor_label = r'$x- \frac{x^3}{3!}+\frac{x^5}{5!}$'

    n = 36 # number of intervals per unit
    delta_theta = pi/n

    # Compute lists of x and y coordinates for the two graphs
    thetas = [-pi+k*delta_theta for k in range(3*n+1)]
    taylor_ys = [taylor_sine(t) for t in thetas]
    sin_ys = [sin(t) for t in thetas]

    # Plot the curves and (border) axes
    plt.plot(thetas, taylor_ys, color='red', label=taylor_label)
    plt.plot(thetas, sin_ys, color='blue', label= r'$\sin(x)$')
    plt.axis([-pi, 2*pi , -2, 2], 3/4 )


    # draw axes centered at the origin
    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='black')

    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')

    plt.legend(loc='lower right')

    # Display graphs
    plt.show()


# This doesn't seem to be working.
plt.savefig("taylor_sin.png")



def plot_function(func, startx, endx, starty, endy, increment = 20):
    delta_theta = pi/increment
    # Compute lists of x and y coordinates for the two graphs
    thetas = [-pi+k*delta_theta for k in range(3*increment+1)]
    fx1 = [func(t) for t in thetas]

    # Plot the curves and (border) axes
    plt.plot(thetas, fx1, color='red')
    plt.axis([startx, endx, starty, endy], 3/4)
    # draw axes centered at the origin
    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.legend(loc='lower right')
    # Display graphs
    plt.show()



def plot_functions(func1, func2, title, label1,
                   label2,increment, startx, endx , starty, endy):
    '''
    Function with parameters matching the example provided by the
    proffessor.
    :param func1:
    :param func2:
    :param title:
    :param label1:
    :param label2:
    :param increment:
    :param startx:
    :param endx:
    :param starty:
    :param endy:
    :return:
    '''
    plt.title(title)
    delta_theta = pi/increment
    # Compute lists of x and y coordinates for the two graphs
    thetas = [-pi+k*delta_theta for k in range(3*increment+1)]
    fx1 = [func1(t) for t in thetas]
    fx2 = [func2(t) for t in thetas]

    # Plot the curves and (border) axes
    plt.plot(thetas, fx1, color='red', label=label1)
    plt.plot(thetas, fx2, color='blue', label=label2)
    plt.axis([startx, endx, starty, endy], 3/4)
    # draw axes centered at the origin
    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.legend(loc='lower right')
    # Display graphs
    plt.show()




def plot_poly(poly, start, end, step, title):
    points = ply.drange(start,end, step)
    fx = ply.eval_poly_points(poly, points)
    plt.plot(points, fx,  color='blue', label=title )
    plt.show()



def plot_list(pt_list, startx, endx, starty, endy):
    '''
    Function plots a piece-wise list of points of
    the form [(x_1, x_i ... ) (y_1, y_i ...]
    With plot limits startx, endx, starty, endy
     (TODO: get max/min x/y and use for plot
     parameters)
    :param pt_list: list of the form (x_list, y_list)
    :param startx: plot parameters
    :param endx:
    :param starty:
    :param endy:
    :return:
    '''
    plt.plot(pt_list[0], pt_list[1], 'ro')
    # plt.axis([startx, endx, starty, endy], 3/4)
    # draw axes centered at the origin
    # first a default hline at y=0 that spans the xrange
    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.legend(loc='lower right')
    # Display graphs
    plt.show()



def plot_poly_points2(poly, func1, pt_list, startx, endx, starty, endy, step, increment, title, lab):
    '''
    Plots a polynomial py getting range of points based
    on start/end and a list of the polynomial evaluated
     at those points.
    :param poly:
    :param pt_list:
    :param startx:
    :param endx:
    :param step:
    :param title:
    :return:
    '''
    plt.title(title)

    delta_theta = pi/increment
    # Compute lists of x and y coordinates for the two graphs
    thetas = [-pi+k*delta_theta for k in range(3*increment+1)]
    fx1 = [func1(t) for t in thetas]

    #Plot polynomial
    points = ply.drange(startx, endx, step)
    fx = ply.eval_poly_points(poly, points)
    plt.plot(points, fx, color='blue', label=lab)

    plt.plot(thetas, fx1, color='red', label=r'$1.6e^{-2x}sin(3 \pi x)$')
    plt.plot(pt_list[0], pt_list[1], 'ro', label= "Nodes")

    # Plot the curves and (border) axes
    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.axis([startx, endx, starty, endy], 3/4)
    # plt.axis([startx, endx, starty, endy], 3/4)
    plt.legend(loc = "lower right")
    plt.show()



def plot_poly_points(poly, pt_list, startx, endx, starty, endy, step, increment, title, lab):
    '''
    Plots a polynomial py getting range of points based
    on start/end and a list of the polynomial evaluated
     at those points.
    :param poly:
    :param pt_list:
    :param startx:
    :param endx:
    :param step:
    :param title:
    :return:
    '''
    plt.axis([startx, endx, starty, endy], 3/4)

    points = ply.drange(startx, endx, step)
    fx = ply.eval_poly_points(poly, points)
    plt.plot(points, fx, color='blue', label= lab)
    #print fx, points
    plt.plot(pt_list[0], pt_list[1], 'ro', label= "Nodes")
    # plt.axis([startx, endx, starty, endy], 3/4)
    plt.show()


def plot_piecewise_poly2(poly_list, func1, data, startx, endx, starty, endy, step, increment, title):

    plt.title(title)
    delta_theta = pi/increment
    # Compute lists of x and y coordinates for the two graphs
    thetas = [-pi+k*delta_theta for k in range(3*increment+1)]
    fx1 = [func1(t) for t in thetas]
    plt.plot(thetas, fx1, color='red', label=r'$1.6e^{-2x}sin(3 \pi x)$')

    data_points = tuples_to_list(data)

    plt.axhline(y=0, color='black')
    # second a default vline at x=0 that spans the yrange
    plt.axvline(x=0, color='black')
    plt.axis([startx, endx, starty, endy], 3/4)
    plt.plot(data_points[0], data_points[1], 'ro', label= "Nodes")

    for i in range(0, len(poly_list)):
        points = ply.drange(data[i][0], data[i+1][0], step)
        fx = ply.eval_poly_points(poly_list[i], points)
        plt.plot(points, fx, color= 'blue')

    plt.legend(loc = "lower right")
    plt.show()


def plot_piecewise_poly(poly_list, data, starty, endy, step):
    data_points = tuples_to_list(data)
    plt.axis([data[0][0], data[len(data)-1][0], starty, endy], 3/4)
    plt.plot(data_points[0], data_points[1], 'ro')
    for i in range(0, len(poly_list)):
        points = ply.drange(data[i][0], data[i][0], step)
        fx = ply.eval_poly_points(poly_list[i], points)
        plt.plot(points, fx, color = 'blue')
    plt.show()

#------- some test
# x = [(1,2,3,4,5,6)]
# y = [(3,4,5,3,2,1)]
# plt.plot(x,y, 'bo')
# plt.show()


# turn point tuples into lists for plotting
def tuples_to_list(points):
    xlist = []
    ylist = []
    for pt in points:
        xlist.append(pt[0])
        ylist.append(pt[1])
    ptlist = (xlist, ylist)
    return ptlist


#------- test tuples
#plist = [(1,2,3), (2,3,4)]
#ptlist = tuples_to_list(plist)
#ptlist