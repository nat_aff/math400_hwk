# I separated some linear algebra functions so
# and that file was imported as "la".
# The functions called as la.blah() should be in the code
# Sklar provided.

import linear_algebra as la

# This was just used to see matrices between swaps to
# find when things went wrong

basic = 1
partial = 2
scaled = 3

printing = False

def findPivotRow1(mat, col):
    #    Finds index of the first row with nonzero entry on or
    #    below diagonal.  If there isn't one return(-1).

    epsilon = 10**(-17)
    for row in range(col, la.rows(mat)):
        #        if mat[row][col] != 0:
        if abs(mat[row][col]) > epsilon:
            if printing:
                print "Swap ============"
                print "Inital row:", col
                print "Returned row:", row
                print "======== End Swap"
            return row
    return -1


def rowReduce(M, type, row_maxes):
   '''
    Funciton returns row based on which
    pivot type is chose in type.
    row_maxes is only used for scaled pivoting.
   '''
    # Type 1 = basic
    # Type 2 = partial pivot
    # Type 3 = scaled pivoting.

    N = la.copyMatrix(M)

    cs = len(M) - 1
    #  cs = la.cols(M)-2   # no need to consider last two cols
    rs = la.rows(M)
    for col in range(cs+1):
        if type == 1:
            j = findPivotRow1(N, col)
        elif type == 2:
            j = findPartialPivotRow(N,col)
        elif type == 3:
            j = findScaledPivotRow(N, col, row_maxes)
        if j < 0:
            print("\nrowReduce: No pivot found for column index %d "%(col))
            return N
        else:
            #---- printing matrix
            if printing:
                print("Matrix before reduction")
                la.show(N)
            # ----End printing

            if j != col:
                N = la.swaprows(N, col, j)
            scale = -1.0 / N[col][col]
            for row in range(col+1, rs):
                N = la.addrows(N, col, row, scale * N[row][col])

            # --- check calculations
            if printing:
                print("Matrix after reduction")
                la.show(N)
                # --- End check
    return N






def ge_1(aug, pivot_type):

    #   Given an augmented matrix it returns a list.  The [0]
    #   element is the row-reduced augmented matrix, and
    #   ge_1(aug)[1] is a solution vector.  The solution
    #   vector is empty if there is no unique solution.
    row_maxes = []

    #If scaled pivoting get row scalings
    if pivot_type == 3:
        row_maxes = findRowScalings(aug)
    aug_n = rowReduce(aug, pivot_type, row_maxes)
    if diag_test(aug_n):
        sol = backSub(aug_n)
    else:
        print("\nge_1(): There is no unique solution")
        sol = []
    results = [aug_n, sol]
    return results


# The next two functions support checking a solution.

def getAandb(aug):
    #   Returns the coef. matrix A and the vector b of Ax=b
    m = la.rows(aug)
    n = la.cols(aug)
    A = la.zero(m, n-1)
    b = la.zero(m, 1)
    for i in range(m):
        for j in range(n-1):
            A[i][j] = aug[i][j]

    for i in range(m):
        b[i] = aug[i][n-1]
    Aandb = [A, b]
    return Aandb



#----- Additional functions for partial and scaled Pivoting

def findRowScalings(mat):
    ''' Function returns vector of values representing
    the max vals in each row with index values corresponding
    to row numbers'''
    max_vals = []
    max_val = 0
    for row in mat:
        for i in row:
            curr_val = abs(i)
            if curr_val > max_val:
                max_val = curr_val
        max_vals.append(max_val)
        max_val = 0
    return max_vals


def findScaledPivotRow(mat, col, row_maxes):
    '''
    Funtion returns row with the large
    absolute value in a given column relative
    to the rows absolute max value
    '''
    epsilon = 10**(-17)
    max_row = col
    swapped = False
    max_val = abs(mat[col][col])/row_maxes[col]

    for row in range(col, la.rows(mat)):
        temp_val = abs(mat[row][col])/row_maxes[row]
        if temp_val > max_val:
            max_val = temp_val
            max_row = row
            swapped = True

    # swap values in row_maxes
    if(swapped):
        la.swapItems(col, max_row, row_maxes)
    if max_val > epsilon:
        if printing:
            print "Swap============"
            print "Inital row:", col
            print "Returned row:", max_row
            print "======== End Swap"
        return max_row
    return -1


def findPartialPivotRow(mat,col):
    '''
     Function finds index of the first row with nonzero entry on or
        below diagonal.  If there isn't one return(-1).
    :param mat: an augmented matrix
    :param col: current column number
    :return: the row to swap with the current row, if any
    '''

    epsilon = 10**(-17)
    max_val = epsilon
    #max_val = mat[col][col]
    max_row = col

    for row in range(col, la.rows(mat)):
        #additions for partial pivot
        temp_val = abs(mat[row][col])
        if temp_val > max_val:
            max_val = temp_val
            max_row = col

    if debugging:
        print "max_val", max_val
    if max_val > epsilon:
        if printing:
            print "Swap============"
            print "Inital row:", col
            print "Returned row:", max_row
            print "======== End Swap"
        return max_row
    return -1


