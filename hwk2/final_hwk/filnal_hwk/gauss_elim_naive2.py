
from __future__ import division
import linear_algebra as la
import copy

__author__ = 'nathanael'

#------------------------------------------------
# Change to alter debug or output printing
# Turn printing on to see intermediate solutions
# (Use when running doing assignment problems)
#------------------------------------------------
debugging = False
printing = False


#----------------------------------------
# Below are professor supplied functions
#-----------------------------------------

def findPivotRow1(mat, col):
    #    Finds index of the first row with nonzero entry on or
    #    below diagonal.  If there isn't one return(-1).

    epsilon = 10**(-17)
    for row in range(col, la.rows(mat)):
        #        if mat[row][col] != 0:
        if abs(mat[row][col]) > epsilon:
            if printing:
                print "Swap ============"
                print "Inital row:", col
                print "Returned row:", row
                print "======== End Swap"
            return row
    return -1


def rowReduce(M, type, row_maxes):
    #    return row reduced version of M
    N = la.copyMatrix(M)

    cs = len(M) - 1
    #  cs = la.cols(M)-2   # no need to consider last two cols
    rs = la.rows(M)
    for col in range(cs+1):
        if type == 1:
            j = findPivotRow1(N, col)
        elif type == 2:
            j = findPartialPivotRow(N,col)
            if debugging:
                print("pivot val = ", N[col][j])
        elif type == 3:
            j = findScaledPivotRow(N, col, row_maxes)
        if j < 0:
            print("\nrowReduce: No pivot found for column index %d "%(col))
            return N
        else:
            #---- printing matrix
            if printing:
                print("Matrix before reduction")
                la.show(N)
            # ----End printing

            if j != col:
                N = la.swaprows(N, col, j)
            scale = -1.0 / N[col][col]
            for row in range(col+1, rs):
                N = la.addrows(N, col, row, scale * N[row][col])

            # --- check calculations
            if printing:
                print("Matrix after reduction")
                la.show(N)
                # --- End check
    return N


def backSub(M):

    #   given a row reduced augmented matrix with nonzero
    #   diagonal entries, returns a solution vector

    cs = la.cols(M)-1     # cols not counting augmented col
    sol = [0 for i in range(cs)]   # place for solution vector
    for i in range(1, cs+1):
        row = cs-i     # work backwards
       # if printing:
        #    print "In backSub()-----------"
        #    print "row before reduction: "
        #    la.show_row(M[row])
        sol[row] = ((M[row][cs] - sum([M[row][j]*sol[j] for
                                       j in range(row+1, cs)])) / M[row][row])
        #if printing:
        #    print "row after reduction: "
        #    print sol
        #    print "End backSub()-----------"
    return sol


def diag_test(mat):

    #   Returns True if no diagonal element is zero, False
    #   otherwise.
    for row in range(la.rows(mat)):
        if mat[row][row] == 0:
            return False
    else:
        return True



def ge_1(aug, pivot_type):

    #   Given an augmented matrix it returns a list.  The [0]
    #   element is the row-reduced augmented matrix, and
    #   ge_1(aug)[1] is a solution vector.  The solution
    #   vector is empty if there is no unique solution.
    scaled_pivot = 3
    row_maxes = []

    #If scaled pivoting get row scalings
    if pivot_type == scaled_pivot:
        row_maxes = findRowScalings(aug)
    aug_n = rowReduce(aug, pivot_type, row_maxes)
    if diag_test(aug_n):
        sol = backSub(aug_n)
    else:
        print("\nge_1(): There is no unique solution")
        sol = []
    results = [aug_n, sol]
    return results


# The next two functions support checking a solution.

def getAandb(aug):
    #   Returns the coef. matrix A and the vector b of Ax=b
    m = la.rows(aug)
    n = la.cols(aug)
    A = la.zero(m, n-1)
    b = la.zero(m, 1)
    for i in range(m):
        for j in range(n-1):
            A[i][j] = aug[i][j]

    for i in range(m):
        b[i] = aug[i][n-1]
    Aandb = [A, b]
    return Aandb


def checkSol_1(aug,x):
    #   For aug=[A|b], returns Ax, b, and b-Ax as vectors
    A  = getAandb(aug)[0]
    b  = getAandb(aug)[1]
    x_col_vec = la.vec2colVec(x)
    Ax = la.matMult(A, x_col_vec)
    r  = la.addVectors(b, la.scalarMult(-1.0, la.colVec2vec(Ax)))
    L  = [Ax, b, r]
    return L


#---------------------------------------------------
# Function below this point are new functions
# written by group 1
#---------------------------------------------------

def LUsolve(aug_m):
    '''
    Given an augmented matrix aug_m
    Solving a matrix using LU factorization
    Ly = b, Uy = x
    :param aug_m:
    :return:
    '''
    # get b
    Ab = getAandb(aug_m)
    b = Ab[1]
    if debugging:
        print "b is; ", b
    # get upper and lower
    LU = LUfactorization2(aug_m)
    L = LU[0]
    U = LU[1]
    y = forward_sub(L, b)
    size = len(y)
    y = la.getCol(y, size)
    Uy = la.augment(U, y)
    x = backSub(Uy)
    return x


def LUfactorization2(mat, augmented = True):
    '''
    Takes an augmented matrix and  returns a list [L,U] with
    L the lower trianguar and U the upper triangular matrix

    :param mat: an augmented matrix
    :return: the list [L,U] with L the lower triangular
    and U the upper triangular matrix
    '''

    len_mat = len(mat)

    if augmented:
        Ab = getAandb(mat)
        A = Ab[0]
    else:
        A = copy.deepcopy(mat)

    if debugging:
        print("first A")
        la.show(A)
    # permute vector determines permute vector
    permute_vector= []
    for i in range(len_mat):
        permute_vector.append(i)

    L = la.getZeroMatrix(len_mat)
    U = la.getZeroMatrix(len_mat)

    for i in range(len_mat):
        L[i][i] = 1

    for j in range(len_mat):
        U[0][j] = A[0][j]

    for col in range(0,len_mat -1):
        j = findPivotRow1(A, col)
        if j != col:
            la.swapItems(j, col, permute_vector)
            A = la.swaprows(A, col, j)

        for row in range(col+1, len_mat):
            scale = -1.0 / A[col][col]
            L[row][col] = (-scale) * A[row][col]
            #print("scale", scale)
            A = la.addrows(A, col, row, scale * A[row][col])
            #print("A is")
            if debugging:
                la.show(A)
    return [L, A]


#----- Additional functions for partial and scaled Pivoting

def findRowScalings(mat):
    ''' Function returns vector of values representing
    the max vals in each row with index values corresponding
    to row numbers'''
    max_vals = []
    max_val = 0
    for row in mat:
        for i in row:
            curr_val = abs(i)
            if curr_val > max_val:
                max_val = curr_val
        max_vals.append(max_val)
        max_val = 0
    return max_vals


def findScaledPivotRow(mat, col, row_maxes):
    '''
    Funtion returns row with the large
    absolute value in a given column relative
    to the rows absolute max value
    '''
    epsilon = 10**(-17)
    max_row = col
    swapped = False

    max_val = abs(mat[col][col])/row_maxes[col]

    for row in range(col, la.rows(mat)):
        temp_val = abs(mat[row][col])/row_maxes[row]
        if temp_val > max_val:
            max_val = temp_val
            max_row = row
            swapped = True

    # swap values in row_maxes
    if(swapped):
        la.swapItems(col, max_row, row_maxes)
    if debugging:
        print "new max_val", max_val
    if max_val > epsilon:
        if printing:
            print "Swap============"
            print "Inital row:", col
            print "Returned row:", max_row
            print "======== End Swap"
        return max_row
    return -1


def findPartialPivotRow(mat,col):
    '''
     Function finds index of the first row with nonzero entry on or
        below diagonal.  If there isn't one return(-1).
    :param mat: an augmented matrix
    :param col: current column number
    :return: the row to swap with the current row, if any
    '''
    #
    epsilon = 10**(-17)
    max_val = epsilon
    #max_val = mat[col][col]
    max_row = col

    for row in range(col, la.rows(mat)):
        #additions for partial pivot
        temp_val = abs(mat[row][col])
        if temp_val > max_val:
            max_val = temp_val
            max_row = col

    if debugging:
        print "max_val", max_val
    if max_val > epsilon:
        if printing:
            print "Swap============"
            print "Inital row:", col
            print "Returned row:", max_row
            print "======== End Swap"
        return max_row
    return -1


def gauss_seidel(aug, max_iter = 50, r_tol = 0.00001, x_tol = 0.00001):
    '''
    Given a starting vector the function iteratively
    finds a solution vector x until the max norm of
    b - AX < tolerance (r_tol) or until the number
    of iterations is >= max_iter(ations)
    :param aug: An augmented matrix Ab
    :param start_x: starting x_0 vector
    :param max_iter: maximum number of iterations
    :return: estimated result vector x
    '''
    #TODO: Add x_tolerance check : norm = || x - x^(k-1)
    iterations = 0
    ab_list = getAandb(aug)
    mat = ab_list[0]
    b_vec = ab_list[1]
    # TODO: don't need following line
    curr_x = [0]* len(mat)
    prev_x = copy.deepcopy(curr_x)
    norm_xvar = 0
    # Get lower and upper matrices
    LU = la.sum_LU_factorization(aug)
    L = LU[0]
    U = LU[1]

    id_mat = la.identity_mat(len(mat))
    invL = inverse_lower_mat(L, id_mat)
    invL = invL[1]
    C_k = la.mult_mat_vec(invL, b_vec)
    neg_invL = la.scalarMult(-1, invL)
    # C_k = L^(-1)b
    diff = la.subtractVectors(b_vec, curr_x)
    norm_max_r = la.vec_inf_norm(diff)
    # T_k = -L^(-1)U
    T_k = la.matMult(neg_invL, U)
    if debugging:
        print "T_k :", la.show(T_k), "C_k:", la.show_row( C_k)

    while norm_max_r > r_tol and iterations < max_iter and norm_xvar < x_tol:
        #This loop calculates the following recursive formula:
        #    x_k = T_k * x^(k-1) + C_k
        T_kx = la.mult_mat_vec(T_k, curr_x)
        curr_x = la.addVectors(C_k, T_kx)
        b_result = la.mult_mat_vec(mat, curr_x)

        if debugging:
            print "result of current iteration", la.show_row(b_result)
        norm_xvar = la.vec_inf_norm(la.subtractVectors(curr_x, prev_x))
        norm_max_r = la.vec_inf_norm(la.subtractVectors(b_vec, b_result))
        iterations += 1
        # reset curr_x to prev_x
        prev_x = copy.deepcopy(curr_x)
    # if max_iter reached and residual > residual tolerance print message
    if iterations == max_iter and norm_max_r > r_tol:
        print "Maximum iterations reached without convergence"
    return curr_x

#--------------------------
# Foward sub, inverses
#--------------------------


def backward_sub(AI):
    '''
    AI is a matrix augmented with an inverse matrix
    Function works
    :param AI: augmented matrix
    :param b:
        :return:
    '''

    size = len(AI)
    aug_mat = copy.deepcopy(AI)

    for col in range(size):
        col = size - col-1
        #print "col", col
        for row in range(col):
           # print "row", row
            aug_mat[col] = la.scalar_mult_vec(aug_mat[col], 1/aug_mat[col][col])
            aug_mat = la.addrows(aug_mat, col, row, -aug_mat[row][col])
        aug_mat[0] = la.scalar_mult_vec(aug_mat[0], 1/aug_mat[0][0])

    return aug_mat



def forward_sub(L, b):
    '''
    L_mat is a lower triangular matrix (not augmented)
    Function returns y such that Ly = b
    Function works
    :param L_mat:
    :param b:
        :return:
    '''

    size = len(L)
    aug_mat = copy.deepcopy(L)
    #if (isinstance(b[0], int)):

    aug_mat = la.augment(L, b)

    #elif (len(b) > 1):
        # if its a matrix
    #    aug_mat = la.augment_mat(L, b)
    #else:
    #    print "Vector of zero length"
    #    return -1
    for col in range(size):
        for row in range(col, size - 1):
            aug_mat[row] = la.scalar_mult_vec(aug_mat[row], 1/aug_mat[col][col])
            aug_mat = la.addrows(aug_mat, col, row+1, -aug_mat[row+1][col])
            if debugging:
                la.show(aug_mat)
    aug_mat[size-1] = la.scalar_mult_vec(aug_mat[size-1],aug_mat[size-1][size-1])

    return aug_mat



#--------------------------
#    Current version of inverse_matrix
#   Only tested on one matrix so far
#--------------------------
def inverse_matrix(mat, pivot_type):
    '''
    Takes an unaugmented matrix and returns an inverse
    matrix of input matrix
    param mat: input matrix

    '''
    # get identity matrix of same size and append to mat
    I_mat = la.identity_mat(len(mat))
    aug = la.append_mat(mat, I_mat)

    scaled_pivot = 3
    row_maxes = []

    #If scaled pivoting get row scalings
    if pivot_type == scaled_pivot:
        row_maxes = findRowScalings(aug)
    aug_n = rowReduce(aug, pivot_type, row_maxes)
    if diag_test(aug_n):
        sol = backward_sub(aug_n)
        # get inverse part of augmented matrix
        sol = la.slice_matrix(sol, (len(mat), 2*len(mat)))
    else:
        print("\nge_1(): There is no unique solution")
        sol = []
        return sol
    return sol

#--------------
# Extras: move to linear algebra/
#---------------


def condition_num(mat):
    # TODO: return max row sum for infinty
    inv_mat = inverse_matrix(mat, 3)
    inv_norm = infinity_norm(inv_mat)
    mat_norm = infinity_norm(mat)
    print "inv_norm, mat_norm", inv_norm, mat_norm

    cond_num = inv_norm * mat_norm
    return cond_num


def infinity_norm(mat):
    # calculate norm matrix
    max_sum = 0
    for row in mat:
        curr_sum = abs_sum(row)
        if curr_sum > max_sum:
            max_sum = curr_sum
    return max_sum



def abs_sum(row):
    sum = 0
    for i in row:
        sum += abs(i)
    return sum


#----------------
# Useless function : used in Bad version of LUFactor--
#----------------


def inverse_lower_mat(mat, I_mat):
    if len(mat[0]) > len(mat):
        print "matrix not of dimension N X N"
        return -1
    rows = len(mat)

    for i in range(rows):
        scale = mat[i][i]
        mat[i] = la.scalar_mult_vec(mat[i], 1/scale)
        I_mat[i] = la.scalar_mult_vec(I_mat[i], 1/scale)
        for j in range(i+1, rows):
            scale = -mat[j][i]
            mat = la.addrows(mat, i, j, scale)
            vec_to_add = la.scalar_mult_vec(I_mat[i], scale)
            I_mat[j] = la.addVectors(vec_to_add, I_mat[j])
            # print "result I", I_mat[j]
    return [mat, I_mat]
