
from __future__ import division
import gauss_elim_naive2 as ge
import linear_algebra as la
import hilbert_matrices as hil
__author__ = 'nathanael'


#---------------------------
# Problem #2 test function
#---------------------------
basic = (1, "basic pivoting")
partial = (2, "partial pivoting")
scaled = (3, "partial scaled pivoting")

# number of digits to show:
ndigits = 3

pivot_types = [basic, partial, scaled]


def test_pivot_types(aug_m, pivot_types, display_all=True):
    for i in pivot_types:
        print "-------------------------------"
        print "Start of test for pivot type: ", i[0], " ", i[1]
        AandB = ge.ge_1(aug_m, i[0])

        if display_all:
            print "sol:, ", AandB[1]
        #----- checkSol returns List = Ax, b, r
        result = ge.checkSol_1(aug_m, AandB[1])
        if display_all:
            print "Solution results"
            print "===================================="
            print "Ax :", la.show(result[0])
            print "b : ", la.show_row(result[1])
        print "Error vector is: ", la.show_row(result[2])
        print "infinity_norm of error vector is:", round(la.vec_inf_norm(result[2]), ndigits)
        print "End of test for pivot type: ", i[0], " ", i[1]
        if(display_all):
            print "====================================="
            print "End of tests"


#-----------------------------------------
# Matrix for Problem 2
#-----------------------------------------
A = [[10, 10, 10, 10 ** 17, 10 ** 17],
     [1, 10 ** (-3), 10 ** (-3), 10 ** (-3), 1],
     [1, 1, 10 ** (-3), 10 ** (-3), 2],
     [1, 1, 1, 10 ** (-3), 3]]

reload(ge)
test_pivot_types(A, pivot_types)

print "The first two methods return the same result. The scaled" \
      "pivot method pushes the first row to the bottom. The max" \
      "error is 16 but as a proportion of the number it is a " \
      "very small percent error compared to the other methods."

#---------------------------
# Problem #3 test function
#---------------------------


def create_aug_hilbert(n):
    hilbert_mat = hil.hilbert(n)
    hilbert_b = hil.hilbert_b(n)
    aug_hilbert = la.augment(hilbert_mat, hilbert_b)
    return aug_hilbert


def create_hilberts(n):
    hilberts = []
    for i in range(2,n+1):
        temp_hilbert = create_aug_hilbert(i)
        hilberts.append(temp_hilbert)
    return hilberts



def test_solution_types(aug_m, solution_types, display_all=True):
    for type in solution_types:
        print "-----------------------"
        print "Start of test for solution type: ", type[0], " ", type[1]
        # gauss_seidel = 1
        if type[0] == 1:
            print "seidel"
            sol_x = ge.gauss_seidel(aug_m)
        if type[0] == 2:
            #LU factorization = 2
            print "LU"
            sol_x = ge.LUsolve(aug_m)
        result = ge.checkSol_1(aug_m, sol_x)
        print "Solution results"
        print "-----------------------------"
        #print "Ax : ", la.show(result[0])
        #print "b : ", la.show_row(result[1])
        print "Error vector is: ", la.show_row(result[2])
        print "infinity_norm of error vector is:", la.vec_inf_norm(result[2])
        #print "End of test for solution type: ", type[0], " ", type[1]
        if display_all:
            print "====================================="
            print "End of tests"


#---------------------
# Functions for running problem 3
#---------
def compare_error(matrix_list, pivot_types, solver_types):
    for i in range(len(matrix_list)):
        print "Testing matrix number: ", i
        test_pivot_types(matrix_list[i], pivot_types, False)
        test_solution_types(matrix_list[i], solver_types, False)
        print "End testing of matrix number: ", len(matrix_list[i])
        print "========================================"
    print "END of testing"


def show_matrices(matrix_list):
    for i in range(len(matrix_list)):
        print "Matrix : ", i
        la.show(matrix_list[i])
        print "----------------\n"


#--------------------------------
# Problem 3 code
#----------------------------------
reload(ge)

pivot_types = [partial, scaled]

LU_factor = (2, "LU factorization")
gauss_seidel = (1, "Gauss-Seidel iteration")
solver_types = [gauss_seidel, LU_factor]

# 5 for testing
#hilbert_list5 = create_hilberts(5)
# from n = 2 to 25
hilbert_list25 = create_hilberts(26)

compare_error(hilbert_list25, pivot_types, solver_types)

res = ge.LUsolve(hilbert_list25[2])

#------------------------------------
# Problem 4 functions supporting
# test code
#------------------------------------


def unaug_hilberts(n):
    h_list = []
    for i in range(2,n):
        mat = hil.hilbert(i)
        h_list.append(mat)
    return h_list

def make_hilbert_invs(n):
    inv_list = []
    for i in range(2, n):
        h_inv = hil.hilbert_inv(i)
        inv_list.append(h_inv)
    return inv_list

def make_inv_lists(matrix_list, pivot_type):
    inv_list = []
    for mat in matrix_list:
        inv = ge.inverse_matrix(mat, pivot_type)
        inv
        inv_list.append(inv)
    return inv_list

def show_mat_list(mat_list):
    for mat in mat_list:
        la.show(mat)


def mat_mult_list(mat_list, inv_list):
    id_list = []
    if len(mat_list) != len(inv_list):
        print "List length does not match"
    for i in range(len(mat_list)):
        inv = la.matMult(mat_list[i], inv_list[i])
        id_list.append(inv)
    return id_list


def mat_norm_list(mat_list):
    norm_list = []
    for mat in mat_list:
        norm = ge.infinity_norm(mat)
        norm_list.append(norm)
    return norm_list

def inv_vector(vec):
    inv = []
    for i in vec:
        x = 1/i
        inv.append(x)
    return inv

#------------------------------------

# 1. Find approx inverses for Hilbert
# 2. Multiply and compare to identity

#-------------------------------------
print " PROBLEM 4 code"

# Inverse function takes unaugmented matrices
# so use unaug_hilbert to create these
hilbert_list25 = unaug_hilberts(25)

# create hilbert inverses
hilbert_inv25 = make_hilbert_invs(25)
hilbert_inv25

basic_list = make_inv_lists(hilbert_list25, 1)
scaled_list = make_inv_lists(hilbert_list25, 3)

show_mat_list(basic_list)
show_mat_list(scaled_list)

id_list_basic = mat_mult_list(basic_list, hilbert_list25)
id_list_scaled = mat_mult_list(scaled_list, hilbert_list25)

print "showing error norms for basic pivoting"
basic_norms = mat_norm_list(id_list_basic)
la.show_row(basic_norms, 4)


print "showing error norms for basic pivoting"
scaled_norms = mat_norm_list(id_list_scaled)
la.show_row(scaled_norms, 4)

len(basic_norms)
len(scaled_norms)

print "---------------------------------------"
print " difference between basic norms and scaled norms"
diff= la.subtractVectors(basic_norms, scaled_norms)
la.show_row(diff)

actual_norms = mat_norm_list(hilbert_list25)

inv_actual = inv_vector(actual_norms)

la.show_row(inv_actual)
la.show_row(actual_norms)



#--------  Calculate condition number of hilbert matrices

def condition_number_list(matrix_list):
    result = []
    for mat in matrix_list:
        num = ge.condition_num(mat)
        result.append(num)
    return result

cond_nums = condition_number_list(hilbert_list25)
cond_nums


