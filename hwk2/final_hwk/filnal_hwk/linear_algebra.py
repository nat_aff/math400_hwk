from __future__ import division


__author__ = 'nathanael'

debugging = True

# coding: utf-8

# """
# gauss_elim_1.py
#
# Includes code for functions that do basic vector and
# matrix arithmetic.  Most of these functions support
# the function ge_1(aug) which takes an n by n+1
# augmented matrix and returns a row-reduced matrix and
# an approximate solution vector of the corresponding linear
# system.  It uses gaussian elimination with a naive pivot
# strategy.  That is, at each stage in the row reduction it
# chooses, as the pivot, the first nonzero entry that lies
# on or below the diagonal in the current column.
#
# revision 2.00 03/12/12  changed filename, commented out tests [ds]
# revision 2.00 02/17/15  fixed test for zero entry in findPivotrow1 [ds]
#
# matrix examples: [[28, 12, 20, 28], [4, 32, 28, 16]] , 2 by 4
# [[28, 12, 20, 28]] , 1 by 4
#                  [[[28], [12], [20], [28]] , 4 by 1
#
#
# vector example [28, 12, 20, 28]
#
# """

# # Supporting functions for Naive Gaussian Elimination function ge_1


def rows(mat):
    #   "return number of rows"
    return (len(mat))


def cols(mat):
    #    "return number of cols"
    return (len(mat[0]))


def zero(m, n):
    #    "Create zero matrix"
    new_mat = [[0 for col in range(n)] for row in range(m)]
    return new_mat


def transpose(mat):
    #    "return transpose of mat"
    new_mat = zero(cols(mat), rows(mat))
    for row in range(rows(mat)):
        for col in range(cols(mat)):
            new_mat[col][row] = mat[row][col]
    return (new_mat)


def dot(A, B):
    #    "vector dot product"
    if len(A) != len(B):
        print("dot: list lengths do not match")
        return ()
    dot = 0
    for i in range(len(A)):
        dot = dot + A[i] * B[i]
    return (dot)


def getCol(mat, col):
    #    "return column col from matrix mat"
    return ([r[col] for r in mat])


def getRow(mat, row):
    #    "return row row from matrix mat"
    return (mat[row])


def matMult(mat1, mat2):
    #    "multiply two matrices"
    if cols(mat1) != rows(mat2):
        print("matMult: mismatched matrices")
        return ()
    prod = zero(rows(mat1), cols(mat2))
    for row in range(rows(mat1)):
        for col in range(cols(mat2)):
            prod[row][col] = dot(mat1[row], getCol(mat2, col))
    return (prod)


def vectorQ(V):
    #    "mild test to see if V is a vector"
    if type(V) != type([1]):
        return (False)
    if type(V[0]) == type([1]):
        return False
    return True


def scalarMult(a, mat):
    #    "multiply a scalar times a matrix"
    if vectorQ(mat):
        return [a * m for m in mat]
    for row in range(rows(mat)):
        for col in range(cols(mat)):
            mat[row][col] = a * mat[row][col]
    return mat


def addVectors(A, B):
    #   "add two vectors"
    if len(A) != len(B):
        print("addVectors: different lengths")
        return ()
    return ([A[i] + B[i] for i in range(len(A))])


def subtractVectors(A, B):
    # subtract two vectors
    x = []
    for i in B:
        x.append(-i)
    res = addVectors(A, x)
    return res

def swaprows(M, i, j):
    #    "swap rows i and j in matrix M"
    N = copyMatrix(M)
    T = N[i]
    N[i] = N[j]
    N[j] = T
    return N


def copyMatrix(M):
    return ([[M[row][col] for col in range(cols(M))] for row in
             range(rows(M))])


def addrows(M, f, t, scale=1):
    #    "add scale times row f to row t"
    N = copyMatrix(M)
    T = addVectors(scalarMult(scale, N[f]), N[t])
    N[t] = T
    return (N)


def show(mat, ndigits = 3):
    '''
    Display's a matrix with floats rounded to
    the ndigits
    :param mat: The matrix to format
    :param ndigits: The number of digits to round to
    :return:
    '''
    for row in mat:
        row = format_vector(row, ndigits)
        print row


def show_row(vec, ndigits=3):
    '''
    Format and print row
    :param vec: row vector to print
    :praram ndigits: number of digits to display
    :return:
    '''
    vec = format_vector(vec, ndigits)
    print vec


def vec2rowVec(vec):
    #    "[a,b,c] -> [[a,b,c]]"
    return ([vec])


def vec2colVec(vec):
    return (transpose(vec2rowVec(vec)))


def colVec2vec(mat):
    rowVec = transpose(mat)
    return (rowVec[0])


def augment(mat, vec):
    #    "given nxn mat and n length vector return augmented matrix"
    amat = []
    for row in range(rows(mat)):
        amat.append(mat[row] + [vec[row]])
    return (amat)


def augment_mat(l_mat, r_mat):
    cols = len(r_mat[0])
    for col in range(cols):
        vec = [row[col] for row in r_mat]
        l_mat = augment(l_mat, vec)
        #show(l_mat)

    return l_mat

#-----------------------
# Additional functions
#-----------------------

def identity_mat(size):
    '''
    Create identity matrix of same dimensions as mat
    :param mat:
    :return: the identity matrix
    '''
    i_mat = zero(size, size)
    for i in range(size):
        i_mat[i][i] = 1
    return i_mat

def append_mat(A, B):
    len_A = len(A)
    len_B = len(B)

    result = []
    if (len_A == len_B):
        for i in range(len_A):
            result.append(A[i] + B[i])
        return result
    else:
        print "Matrices not of equal length"
        return [A, B]


def scalar_mult_vec(vec, scalar):
    '''
    Multliply a vector by  a scalar
    :param vec: the vector
    :param scalar:
    :return: vector & scalar
    '''
    x = []
    for i in vec:
        x.append(scalar * i)
    return x



def slice_matrix(mat, col_range):
    '''
    TODO: comment
    Gets col_range of a matrix
    starting col(0, 1)
    :param mat:
    :param col_range:
    :return:
    '''
    start = col_range[0]
    end = col_range[1]
    vec = []
    result = []
    for i in range(start, end):
        result.append(vec)
    for i in range(start, end):
        vec = [row[i] for row in mat]
        result = augment(result, vec)
    return result


def mult_mat_vec(mat, vec):
    '''

    :param mat:
    :param vec:
    :return:
    '''
    col_vec = vec2colVec(vec)
    Ax = matMult(mat, col_vec)
    row_vec = [row[0] for row in Ax]
    return row_vec


def sum_LU_factorization(aug_mat):
    '''
    Return [L, U] such that L + U = A
    where aug_mat = Ab
    :param aug_mat: Ab
    :return: [L, U]
    '''
    size = len(aug_mat)
    L = zero(size, size)
    U = zero(size, size)
    for i in range(len(aug_mat)):
        for j in range(len(aug_mat)):
            if j <= i:
                L[i][j] = aug_mat[i][j]
            else:
                U[i][j] = aug_mat[i][j]
    return [L, U]

def add_matrices(A,B):
    '''
    return A + B
    :param A:
    :param B:
    :return: (A + B)
    '''
    size = len(A)
    result = zero(size, size)

    for i in range(size):
        for j in range(size):
            result[i][j] = A[i][j] + B[i][j]
    return result



def get_max_r(A, x, b):
    '''
    Ax -b =r
    returns max_norm (r)
    :param A:
    :param x:
    :param b:
    :return:
    '''
    b_res = matMult(A, x)
    r = subtractVectors(b, b_res)
    max_norm = vec_inf_norm(r)
    return max_norm


def vec_inf_norm(int_list):
    '''
    Get
    :param int_list:
    :return:
    '''
    list_max = max(max(int_list), min(int_list), key=abs)
    return abs(list_max)



def swapItems(i, j, vec):
    '''
    Swaps items i, j in a vector
    :param i:
    :param j:
    :param vec:
    :return:
    '''
    temp = vec[i]
    vec[i] = vec[j]
    vec[j] = temp
    return vec


''' redundant = zero in linear_algebra '''
def getZeroMatrix(N):
    row = [0]*N
    mat = []
    for i in row:
        mat.append(list(row))
    return mat


def format_vector(vec, ndigits=3):
    '''
    Return a vector with numbers rounded to n
    decimals
    :param ndigts: Number of decimals to
    :param vec: list of numbers to round
    :return:
    '''

    formatted = []
    for num in vec:
        num = round(num, ndigits)
        formatted.append(num)
    return formatted


